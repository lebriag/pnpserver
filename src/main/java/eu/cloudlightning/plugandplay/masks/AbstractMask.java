/*
 * Set of DB table names (or collections).
 */
package eu.cloudlightning.plugandplay.masks;

import eu.cloudlightning.bcri.clresource.CLResource;
import eu.cloudlightning.plugandplay.resourcemodel.ResourceDescription;
import org.json.simple.JSONObject;

/**
 *
 * @author Gabriel Gonzalez
 */
 public abstract class AbstractMask {

    public abstract CLResource createCLResource(ResourceDescription registrationDescription, String componentID, String clResourceID);

}
