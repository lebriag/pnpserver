/*
 * Set of DB table names (or collections).
 */
package eu.cloudlightning.plugandplay.masks;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.cloudlightning.bcri.blueprint.BareMetalResource;
import eu.cloudlightning.bcri.blueprint.DFEResourceManagerResource;
import eu.cloudlightning.bcri.blueprint.MarathonResourceManagerResource;
import eu.cloudlightning.bcri.blueprint.NUMAResourceManagerResource;
import eu.cloudlightning.bcri.blueprint.OpenStackAccountResource;
import eu.cloudlightning.bcri.blueprint.OpenStackResource;
import eu.cloudlightning.bcri.blueprint.OpenStackVMResource;
import eu.cloudlightning.bcri.blueprint.RocksResourceManagerResource;
import eu.cloudlightning.bcri.blueprint.ServiceImplementationType;
import eu.cloudlightning.bcri.clresource.CLResource;
import eu.cloudlightning.bcri.clresource.ComputationUnitType;
import eu.cloudlightning.bcri.clresource.GenericResourceUnit;
import eu.cloudlightning.bcri.clresource.ResourceDescriptor;
import eu.cloudlightning.bcri.clresource.ServerAccess;
import eu.cloudlightning.bcri.event.EventResourceRegistration;
import eu.cloudlightning.plugandplay.resourcemodel.Body;
import eu.cloudlightning.bcri.event.ResourceType;
import eu.cloudlightning.bcri.event.EventType;
import eu.cloudlightning.plugandplay.resourcemodel.ResourceDescription;

/**********************************************
 * @author Gabriel Gonzalez
 **********************************************/


 public class BasicMask extends AbstractMask {

    public CLResource createCLResource(ResourceDescription resourceDescription, String componentID, String clResourceID){
        
    	final String PATH_TO_ACCELERATOR = "/sys/class/";
    	double timestamp = System.currentTimeMillis();
    	boolean ocuppationalstatuscheck = false;
        //long timestamp = System.currentTimeMillis();
        
        //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        // Resource type
        //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        CLResource clRes = new CLResource();
             
        clRes.setClResourceId(clResourceID);
        clRes.setTimestamp(timestamp);
        
        //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        // Resource type
        //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        if (resourceDescription.getHead() == null) {
                System.out.println("BasicMask[createCLResource]: Registration file has to contain a head. ");
                return null;
        }
        if (resourceDescription.getHead() != null) {
            if ((resourceDescription.getHead().getManager() == null) || 
                (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("SSH"))){
            	clRes.setResourceType(ResourceType.BAREMETAL.toString());                 // BAREMETAL,
            }
            else if (((resourceDescription.getHead().getManager().getType().toUpperCase().contains("ACCOUNT")) || 
             (resourceDescription.getHead().getManager().getType().toUpperCase().contains("NOVA")))){
            	ocuppationalstatuscheck = true;
               	clRes.setResourceType(ResourceType.OPENSTACK_ACCOUNT.toString());// OPENSTACK_ACCOUNT, 
            } 
            else if (resourceDescription.getHead().getManager().getType().toUpperCase().contains("RESOURCE")){ 
            	ocuppationalstatuscheck = true;
                clRes.setResourceType(ResourceType.OPENSTACK_RESOURCE.toString());// OPENSTACK_RESOURCE, 
            }
            else if (((resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("VM")))){
                   	clRes.setResourceType(ResourceType.OPENSTACK_VM.toString());     // OPENSTACK_VM
            } 
            else if (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("ROCKS")){
            	clRes.setResourceType(ResourceType.ROCKS_RESOURCE_MANAGER.toString());    // ROCKS,
            } 
            else if (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("MARATHON")){
            	clRes.setResourceType(ResourceType.MARATHON_RESOURCE_MANAGER.toString()); // MARATHON,
            } 
            else if (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("MAXOS")){
            	clRes.setResourceType(ResourceType.DFE_RESOURCE_MANAGER.toString());      // DFE_RESOURCE_MANAGER,
            } 
            else if (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("NUMA_RESOURCE_MANAGER")){
            	clRes.setResourceType(ResourceType.NUMA_RESOURCE_MANAGER.toString());      // NUMA_RESOURCE_MANAGER,
            } else {
                System.out.println("BasicMask[createCLResource]: Unknown CL-Resource Type: " + resourceDescription.getHead().getManager().getType().toString());
                return null;
            }
            
        } else {
                System.out.println("BasicMask[createCLResource]: Registration file has to contain a head. ");
                return null;
        }
    

        ServerAccess sa = new ServerAccess();
        ResourceDescriptor rd = new ResourceDescriptor(); 
        
        // Telemetry
        if (resourceDescription.getHead().getTelemetry() != null){
            clRes.setTelemetryEndpoint(resourceDescription.getHead().getTelemetry().getUrl() + ":" + resourceDescription.getHead().getTelemetry().getPort());
//            clRes.setTelemetryType (resourceDescription.getHead().getTelemetry().getType());
        } else {
            System.out.println("BasicMask[createCLResource]: The CL-Resource needs a telemetry endpoint.");
            return null;
        }
            
        // Deployment
        Gson gson = new GsonBuilder().create();
        if (resourceDescription.getHead().getDeployment() != null){
            //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx Server Access xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx                       
            sa.setUsername(resourceDescription.getHead().getDeployment().getUsername());
            sa.setPassword(resourceDescription.getHead().getDeployment().getPassword());
            sa.setAccessKey(resourceDescription.getHead().getDeployment().getAccesskey());
            //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            String resourceDescriptorString = "";
        	 if ((resourceDescription.getHead().getManager() == null) || 
                 (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("SSH"))){
        		 BareMetalResource bmres = new BareMetalResource();
        		 bmres.setUsername(resourceDescription.getHead().getDeployment().getUsername());
        		 bmres.setPassword(resourceDescription.getHead().getDeployment().getPassword());
        		 bmres.setSshKey(resourceDescription.getHead().getDeployment().getAccesskey());
        		 bmres.setIpAddress(resourceDescription.getHead().getDeployment().getDeploymentUrl());
	       		 resourceDescriptorString = gson.toJson(bmres);
             }  
             else if (resourceDescription.getHead().getManager().getType().toUpperCase().contains("OPENSTACK_ACCOUNT")){
                	 OpenStackAccountResource bmres = new OpenStackAccountResource();
    	       		 bmres.setUsername(resourceDescription.getHead().getDeployment().getUsername());
    	       		 bmres.setPassword(resourceDescription.getHead().getDeployment().getPassword());
    	       		 bmres.setAuthEndpoint(resourceDescription.getHead().getDeployment().getDeploymentUrl());
    	       		 bmres.setSshKey(resourceDescription.getHead().getDeployment().getAccesskey());
    	       		 bmres.setDomain(resourceDescription.getHead().getDeployment().getUserdomainname());
    	       		 bmres.setProject(resourceDescription.getHead().getDeployment().getProjectid());
    	       		 bmres.setPlatform(clRes.getResourceType());
    	       		 bmres.setDomain(resourceDescription.getHead().getDeployment().getUserdomainname());
    	       		 bmres.setProject(resourceDescription.getHead().getDeployment().getProjectid());
    	       		 resourceDescriptorString = gson.toJson(bmres);               
             }
             else if (resourceDescription.getHead().getManager().getType().toUpperCase().contains("OPENSTACK_RESOURCE")){
                       	 OpenStackResource bmres = new OpenStackResource();
           	       		 bmres.setUsername(resourceDescription.getHead().getDeployment().getUsername());
           	       		 bmres.setPassword(resourceDescription.getHead().getDeployment().getPassword());
           	       		 bmres.setAuthEndpoint(resourceDescription.getHead().getDeployment().getDeploymentUrl());
           	       		 bmres.setSshKey(resourceDescription.getHead().getDeployment().getAccesskey());
           	       		 bmres.setDomain(resourceDescription.getHead().getDeployment().getUserdomainname());
           	       		 bmres.setProject(resourceDescription.getHead().getDeployment().getProjectid());
           	       		 bmres.setPlatform(clRes.getResourceType());
           	       		 bmres.setDomain(resourceDescription.getHead().getDeployment().getUserdomainname());
           	       		 bmres.setProject(resourceDescription.getHead().getDeployment().getProjectid());
           	       		 resourceDescriptorString = gson.toJson(bmres);               
            } 
             else if (((resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("VM")) || 
                     (resourceDescription.getHead().getManager().getType().toUpperCase().contains("OPENSTACK_VM")))){
            	 OpenStackVMResource bmres = new OpenStackVMResource();
	       		 bmres.setUsername(resourceDescription.getHead().getDeployment().getUsername());
	       		 bmres.setPassword(resourceDescription.getHead().getDeployment().getPassword());
	       		 bmres.setSshKey(resourceDescription.getHead().getDeployment().getAccesskey());
	       		 bmres.setIpAddress(resourceDescription.getHead().getDeployment().getDeploymentUrl());
	       		 resourceDescriptorString = gson.toJson(bmres);
            	 
             }
             else if (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("ROCKS")){
            	 RocksResourceManagerResource bmres = new RocksResourceManagerResource();
	       		 bmres.setUsername(resourceDescription.getHead().getDeployment().getUsername());
	       		 bmres.setPassword(resourceDescription.getHead().getDeployment().getPassword());
	       		 bmres.setSshKey(resourceDescription.getHead().getDeployment().getAccesskey());
	       		 bmres.setIpAddress(resourceDescription.getHead().getDeployment().getDeploymentUrl());
	       		 resourceDescriptorString = gson.toJson(bmres);
             } 
             else if (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("MARATHON")){
            	 MarathonResourceManagerResource bmres = new MarathonResourceManagerResource();
	       		 bmres.setUsername(resourceDescription.getHead().getDeployment().getUsername());
	       		 bmres.setPassword(resourceDescription.getHead().getDeployment().getPassword());
	       		 bmres.setEndpoint(resourceDescription.getHead().getDeployment().getDeploymentUrl());
	       		 bmres.setAuthKey(resourceDescription.getHead().getDeployment().getAccesskey());
	       		 String bmresstr = "";
	       		if (resourceDescription.getBody() != null) {
	                for (Body element : resourceDescription.getBody()) {
			       		if (element.getHardware().getCoProcessors()!= null){
			       			// MIC
			       			if (element.getHardware().getCoProcessors().getMic().size() >= 1) {
			       				int size  = element.getHardware().getCoProcessors().getMic().size();
			       			    for (int i = 0; i < size -1 ; i++) {	               			 
		               			   bmresstr = bmresstr + PATH_TO_ACCELERATOR + "mic/" + element.getHardware().getCoProcessors().getMic().get(i).getId() + ";";
	               			    }
			       			    bmres.setAcceleratorMountingPoint(bmresstr + PATH_TO_ACCELERATOR + "mic/" + element.getHardware().getCoProcessors().getMic().get(size-1).getId());
			       			}

			       			// GPU
			       			if (element.getHardware().getCoProcessors().getGpu().size() >= 1) {
			       				int size  = element.getHardware().getCoProcessors().getGpu().size();
			       			    for (int i = 0; i < size -1 ; i++) {	               			 
			       			    	bmresstr = bmresstr + PATH_TO_ACCELERATOR + "gpu" + element.getHardware().getCoProcessors().getGpu().get(i).getId() + ";";
	               			    }
			       			    bmresstr = "nvidia_driver_384.90";
			       			    bmres.setAcceleratorMountingPoint(bmresstr);
//			       			    bmres.setAcceleratorMountingPoint(bmresstr + PATH_TO_ACCELERATOR + "gpu" + element.getHardware().getCoProcessors().getGpu().get(size-1).getId());
			       			    
			       			}
			       			
			       			// DFE
			       			if (element.getHardware().getCoProcessors().getDfe().size() >= 1) {
			       				int size  = element.getHardware().getCoProcessors().getDfe().size();
			       			    for (int i = 0; i < size -1 ; i++) {	               			 
			       			    	bmresstr = bmresstr + PATH_TO_ACCELERATOR + "dfe" + element.getHardware().getCoProcessors().getDfe().get(i).getId() + ";";
	               			    }
			       			    bmres.setAcceleratorMountingPoint(bmresstr +PATH_TO_ACCELERATOR + "dfe" + element.getHardware().getCoProcessors().getDfe().get(size-1).getId());
			       			}		       			
		               		 break;
			       		}
	                }
	       		}
	       		 resourceDescriptorString = gson.toJson(bmres);
             } 
             else if (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("MAXOS")){
        		 DFEResourceManagerResource bmres = new DFEResourceManagerResource();
        		 bmres.setUsername(resourceDescription.getHead().getDeployment().getUsername());
        		 bmres.setPassword(resourceDescription.getHead().getDeployment().getPassword());
        		 bmres.setSshKey(resourceDescription.getHead().getDeployment().getAccesskey());
        		 bmres.setIpAddress(resourceDescription.getHead().getDeployment().getDeploymentUrl());
	       		 resourceDescriptorString = gson.toJson(bmres);
             } 
             else if (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("NUMA_RESOURCE_MANAGER")){
        		 NUMAResourceManagerResource bmres = new NUMAResourceManagerResource();
        		 bmres.setUsername(resourceDescription.getHead().getDeployment().getUsername());
        		 bmres.setPassword(resourceDescription.getHead().getDeployment().getPassword());
        		 bmres.setSshKey(resourceDescription.getHead().getDeployment().getAccesskey());
        		 bmres.setIpAddress(resourceDescription.getHead().getDeployment().getDeploymentUrl());
	       		 resourceDescriptorString = gson.toJson(bmres);
             } else {
                 System.out.println("BasicMask[createCLResource]: Unknown CL-Resource Type: " + resourceDescription.getHead().getManager().getType().toString());
                 return null;
             }

            // Resource Descriptor
        	 clRes.setResourceDescriptor(resourceDescriptorString);

        } else {
                System.out.println("BasicMask[createCLResource]: Registration file has to contain an access method to the resource. ");
                return null;
        }
       
        if (resourceDescription.getBody() != null) {
            for (Body element : resourceDescription.getBody()) {

                GenericResourceUnit gru = new GenericResourceUnit();

                gru.setGenericResourceUnitId(element.getHostName());
                gru.setIpAddress(element.getNodeIp());
                gru.setOperatingSystem(element.getSoftware().getOs().getOsType());
                gru.setOperatingSystemVersion(element.getSoftware().getOs().getOsDistribution());
                gru.setServerName(element.getHostName());

                gru.setServerAccess(sa);

                // CPU             
                eu.cloudlightning.bcri.clresource.CPU cpu = new eu.cloudlightning.bcri.clresource.CPU();
                
                if (element.getHardware().getCpu()!= null) {
                    cpu.setModel(element.getHardware().getCpu().getModelName());
                    cpu.setManufacturer(element.getHardware().getCpu().getVendorId());
                    cpu.setProcessorType(element.getHardware().getCpu().getArchitecture());
                    cpu.setBusWidth(element.getHardware().getCpu().getCpuFrequency().intValue());
                    cpu.setCacheSize(0);
                    cpu.setCurrentAvailableCores(element.getHardware().getCpu().getThreadPerCore()*element.getHardware().getCpu().getCoresPerSocket());
                    cpu.setFrequency(element.getHardware().getCpu().getCpuFrequency());
                    cpu.setTotalNumOfCores(element.getHardware().getCpu().getThreadPerCore()*element.getHardware().getCpu().getCoresPerSocket());
                    cpu.setTotalNumOfProcessor(element.getHardware().getCpu().getCoresPerSocket());
                }
                else if (element.getHardware().getCoProcessors().getDfe() == null){ // Only MPCX node can has not cpu
                    System.out.println("BasicMask[createCLResource]: The registration has no CPU and is not an MPCX node ");
                    return null;    
                }
                
                gru.setCpu(cpu);

                // Memory
                if (element.getHardware().getMemory()!= null) {
                    eu.cloudlightning.bcri.clresource.Memory mem = new eu.cloudlightning.bcri.clresource.Memory();
                    mem.setMemoryType(element.getHardware().getMemory().getRamType());
                    mem.setCurrentAvailableSize(element.getHardware().getMemory().getRamCapacity());
                    mem.setTotalSize(element.getHardware().getMemory().getRamCapacity());
                    gru.setMemory(mem);
                } else if (element.getHardware().getCoProcessors().getDfe() == null){ // Only MPCX node can has not cpu
                    System.out.println("BasicMask[createCLResource]: The registration has no CPU and is not an MPCX node ");
                    return null;    
                }

                // Network
                if (element.getHardware().getNetwork()!= null) {
                    for (eu.cloudlightning.plugandplay.resourcemodel.Network net : element.getHardware().getNetwork()) {
                        eu.cloudlightning.bcri.clresource.Network nw = new eu.cloudlightning.bcri.clresource.Network();
                        nw.setNetworkType(net.getType());
                        nw.setCurrentAvailableBandwidth(net.getSpeed());
                        nw.setTotalBandwidth(net.getSpeed());
                        gru.getNetwork().add(nw);
                    }
                }  else {
                    System.out.println("BasicMask[createCLResource]: The resource type needs a network ");
                    return null;
                }
                
                // Storage 
                if (element.getHardware().getStorage()!= null) {
                    for (eu.cloudlightning.plugandplay.resourcemodel.Storage st : element.getHardware().getStorage()) {
                        eu.cloudlightning.bcri.clresource.Storage strg = new eu.cloudlightning.bcri.clresource.Storage();
                        strg.setStorageType(st.getType());
                        strg.setRpm(5400);
                        strg.setTotalSize(st.getCapacity());
                        strg.setStorageType(st.getType());
                        strg.setCurrentAvailableSize(st.getCapacity());
                        gru.getStorage().add(strg);
                    }
                } 
                
                // Coprocessors
                if (element.getHardware().getCoProcessors()!= null) {
                    // MIC
                    if (element.getHardware().getCoProcessors().getMic()!= null) {                 	
                    	
                        for (eu.cloudlightning.plugandplay.resourcemodel.Mic coprocessor: element.getHardware().getCoProcessors().getMic()) {
                            eu.cloudlightning.bcri.clresource.Accelerator acc = new eu.cloudlightning.bcri.clresource.Accelerator();
                            acc.setAcceleratorType(ComputationUnitType.MIC.toString());                            
                            if (coprocessor.getPciSpeed() == null)
                            	acc.setBusType("");
                            else
                            	acc.setBusType(coprocessor.getPciSpeed().toString());
                            acc.setManufacturer("Intel");
                            acc.setModel(coprocessor.getModel());
                            if (coprocessor.getFrequencyCores() == null)
                            	acc.setFrequency(0);
                            else
                            	acc.setFrequency(coprocessor.getFrequencyCores());
                            if (coprocessor.getMemory() == null)
                            	acc.setMemorySize(0);
                            else
                            	acc.setMemorySize(coprocessor.getMemory());
                            if (coprocessor.getNumCores() == null)
                            	acc.setNumOfCores(0);
                            else
                            	acc.setNumOfCores(coprocessor.getNumCores());
                            
                            String coprocessorId = "";               			 
                            coprocessorId = PATH_TO_ACCELERATOR + "mic/" + coprocessor.getId();
                            acc.setAcceleratorId(coprocessorId);
                            
                            gru.getAccelerator().add(acc);
                        }
                    }

                    // GPU
                    if (element.getHardware().getCoProcessors().getGpu().size() != 0) {
                        for (eu.cloudlightning.plugandplay.resourcemodel.Gpu coprocessor: element.getHardware().getCoProcessors().getGpu()) {
                            eu.cloudlightning.bcri.clresource.Accelerator acc = new eu.cloudlightning.bcri.clresource.Accelerator();
                            acc.setAcceleratorType(ComputationUnitType.GPU.toString());
                            acc.setBusType(coprocessor.getBusId());
                            acc.setManufacturer(coprocessor.getProductBrand());
                            acc.setModel(coprocessor.getProductName());
                            acc.setMemoryClock(coprocessor.getApplicationsClockMemory());
                            acc.setMemorySize(coprocessor.getMemoryTotal());
                            String coprocessorId = "";
                            coprocessorId = "nvidia_driver_384.90";		       			    
                            //coprocessorId = PATH_TO_ACCELERATOR + "gpu" + coprocessor.getId();
                            acc.setAcceleratorId(coprocessorId);
                            gru.getAccelerator().add(acc);
                        }
                    }

                    // DFE
                    if (element.getHardware().getCoProcessors().getDfe().size() != 0) {
                        for (eu.cloudlightning.plugandplay.resourcemodel.Dfe coprocessor: element.getHardware().getCoProcessors().getDfe()) {
                            eu.cloudlightning.bcri.clresource.Accelerator acc = new eu.cloudlightning.bcri.clresource.Accelerator();
                            acc.setAcceleratorType(ComputationUnitType.DFE.toString());
                            acc.setManufacturer(coprocessor.getFpgaVendor());
                            acc.setMemoryType(coprocessor.getFmemType());
                            acc.setModel(coprocessor.getModel());
                            acc.setMemoryClock(coprocessor.getLmemMaxClockRate());
                            acc.setMemorySize(coprocessor.getLmemCapacity());
                            String coprocessorId = "";               			 
                            coprocessorId = PATH_TO_ACCELERATOR + "dfe" + coprocessor.getId();
                            acc.setAcceleratorId(coprocessorId);
                            gru.getAccelerator().add(acc);
                        }
                    }
                }
                
                
                clRes.getGenericResourceUnits().add(gru);

                // Resource type
                String resourceTypeDescription = "";
                if (resourceDescription.getHead() != null) {
                    if ((resourceDescription.getHead().getManager() == null) || (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("SSH"))){
                    	if (element.getHardware().getCoProcessors()== null){
                    		resourceTypeDescription = ServiceImplementationType.CPU_BM.toString(); 
                    	} 
                    	else {
                    		 if (element.getHardware().getCoProcessors().getMic().size() != 0) {
                    			 resourceTypeDescription = ServiceImplementationType.MIC_BM.toString(); 
                             }

                             // GPU
                    		 else if (element.getHardware().getCoProcessors().getGpu().size() != 0) {
                    			 resourceTypeDescription = ServiceImplementationType.GPU_BM.toString(); 
                             }

                             // DFE
                    		 else if (element.getHardware().getCoProcessors().getDfe().size() != 0) {
                    			 resourceTypeDescription = ServiceImplementationType.DFE_BM.toString(); 
                             }
                    		 else{
                    			 System.out.println("BasicMask[createCLResource]: Registration file error resource description descriptor. ");
                    			 return null;
                    		 }
                         }                		
                    	
                    }
                    else if (((resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("OPENSTACK")) || 
                     (resourceDescription.getHead().getManager().getType().toUpperCase().contains("NOVA")))){
                    	if (element.getHardware().getCoProcessors()== null){
                    		resourceTypeDescription = ServiceImplementationType.CPU_VM .toString(); 
                    	}
                    	else {
	                   		 if (element.getHardware().getCoProcessors().getMic().size() != 0) {
	                   			resourceTypeDescription = ServiceImplementationType.MIC_VM.toString(); 
	                            }
	
	                            // GPU
	                   		 else if (element.getHardware().getCoProcessors().getGpu().size() != 0) {
	                   			resourceTypeDescription = ServiceImplementationType.GPU_VM.toString(); 
	                            }
	
	                            // DFE
	                   		 else if (element.getHardware().getCoProcessors().getDfe().size() != 0) {
	                   			resourceTypeDescription = ServiceImplementationType.DFE_VM.toString(); 
	                            }
	                   		 else{
	                   			 System.out.println("BasicMask[createCLResource]: Registration file error resource description descriptor. ");
	                   			 return null;
	                   		 }
                        }      

                    } 
                    else if (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("ROCKS")){
                    	//clRes.setResourceType(ResourceType.ROCKS_RESOURCE_MANAGER.toString());    // ROCKS,
                    	if (element.getHardware().getCoProcessors()== null){
                    		resourceTypeDescription = ServiceImplementationType.CPU_QUEUE.toString(); // MARATHON,
                    	}
                    	else {
	                   		 if (element.getHardware().getCoProcessors().getMic().size() != 0) {
	                   			resourceTypeDescription = ServiceImplementationType.MIC_QUEUE.toString(); 
	                            }
	
	                            // GPU
	                   		 else if (element.getHardware().getCoProcessors().getGpu().size() != 0) {
	                   			resourceTypeDescription = ServiceImplementationType.GPU_QUEUE.toString(); 
	                            }
	
	                            // DFE
	                   		 else if (element.getHardware().getCoProcessors().getDfe().size() != 0) {
	                   			resourceTypeDescription = ServiceImplementationType.DFE_QUEUE.toString(); 
	                            }
	                   		 else{
	                   			 System.out.println("BasicMask[createCLResource]: Registration file error resource description descriptor. ");
	                   			 return null;
	                   		 }
                       }      

                    	
                    } 
                    else if (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("MARATHON")){
                    	if (element.getHardware().getCoProcessors()== null){
                    		resourceTypeDescription = ServiceImplementationType.CPU_CONTAINER.toString(); // MARATHON,
                    	}
                    	else {
	                   		 if (element.getHardware().getCoProcessors().getMic().size() != 0) {
	                   			resourceTypeDescription = ServiceImplementationType.MIC_CONTAINER.toString();	  
	                   			}
	
	                            // GPU
	                   		 else if (element.getHardware().getCoProcessors().getGpu().size() != 0) {
	                   			resourceTypeDescription = ServiceImplementationType.GPU_CONTAINER.toString(); 
	                            }
	
	                            // DFE
	                   		 else if (element.getHardware().getCoProcessors().getDfe().size() != 0) {
	                   			resourceTypeDescription = ServiceImplementationType.DFE_CONTAINER.toString(); 
	                            }
	                   		 else{
	                   			 System.out.println("BasicMask[createCLResource]: Registration file error resource description descriptor. ");
	                   			 return null;
	                   		 }
                       }      

                    } 
                    else if (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("MAXOS")){
                    	resourceTypeDescription = ResourceType.DFE_RESOURCE_MANAGER.toString();      // DFE_RESOURCE_MANAGER,

                    } 
//                    else if (resourceDescription.getHead().getDeployment().getName().toUpperCase().contains("NUMA_RESOURCE_MANAGER")){
//                    	clRes.setResourceType(ResourceType.NUMA_RESOURCE_MANAGER.toString());      // NUMA_RESOURCE_MANAGER,
//                    	if (element.getHardware().getCoProcessors()== null){
//                    		clRes.setResourceType(ServiceImplementationType.NUMA_RESOURCE_MANAGER.toString());
//                    	}
//                    	else {
//	                   		 if (element.getHardware().getCoProcessors().getMic()!= null) {
//	                   			 clRes.setResourceType(ServiceImplementationType.NUMA_RESOURCE_MANAGER.toString()); 
//	                            }
//	
//	                            // GPU
//	                   		 else if (element.getHardware().getCoProcessors().getGpu()!= null) {
//	                           	 clRes.setResourceType(ServiceImplementationType.NUMA_RESOURCE_MANAGER.toString()); 
//	                            }
//	
//	                            // DFE
//	                   		 else if (element.getHardware().getCoProcessors().getDfe()!= null) {
//	                           	 clRes.setResourceType(ServiceImplementationType.NUMA_RESOURCE_MANAGER.toString()); 
//	                            }
//	                   		 else{
//	                   			 System.out.println("BasicMask[createCLResource]: Registration file error resource description descriptor. ");
//	                   			 return null;
//	                   		 }
//                       }      
//
//                    } 
                	else {
                        System.out.println("BasicMask[createCLResource]: Unknown CL-Resource Type: " + resourceDescription.getHead().getManager().getType().toString());
                        return null;
                    }

                    clRes.setClResourceId(resourceTypeDescription);
                    if (!ocuppationalstatuscheck) clRes.setOccupationalStatus("FREE");
                    else  
                    	clRes.setOccupationalStatus("BUSY");
                } else {
                        System.out.println("BasicMask[createCLResource]: Registration file has to contain a head. ");
                        return null;
                }

            }
        }        
        
        return clRes;
    }

}
