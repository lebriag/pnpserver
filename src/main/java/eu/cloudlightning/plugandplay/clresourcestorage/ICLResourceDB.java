/*
 * Interface for the P&P operations
 */
package eu.cloudlightning.plugandplay.clresourcestorage;

import eu.cloudlightning.bcri.clresource.CLResource;

/**
 *
 * @author Gabriel Gonzalez
 */
 public interface ICLResourceDB {

    public Boolean register(CLResource clResource);
    
    public Boolean unregister(String clusterId);
}
