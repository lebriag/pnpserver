/*
 * Plug And Play main operations with the database
 */
package eu.cloudlightning.plugandplay.clresourcestorage;

import eu.cloudlightning.bcri.clresource.CLResource;
import eu.cloudlightning.plugandplay.resourcemodel.CLDBConnector;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author Gabriel Gonzalez Castane
 */
public class PlugAndPlayDBConnector extends CLDBConnector {

    public PlugAndPlayDBConnector(String userIdAuth, String userPwdAuth, String host, String port, String name, String protocol) {        
        super(userIdAuth, userPwdAuth, host, port, name, protocol);
    }
         
    /**
     *
     * @param clResource
     * @return
     */
    public Boolean register(JSONObject clResource) {
        Boolean operationSuccess = true;
        
        // Register the cluster
        super.establishConnection(CLRESOURCES);

        if (operationSuccess) operationSuccess = super.registerElement(clResource,CLRESOURCES);

        // The process has an open connection until the possible rollback would finish
        super.disconnect();
        
        // If recording and rollback fails
        if (!operationSuccess)
            try {
                throw new Exception("Plug and play registration service cannot access to the DB.");
            } catch (Exception ex) {
                Logger.getLogger(PlugAndPlayDBConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        return operationSuccess;
    }         
    
    public Boolean unregister(String clusterId){
        
        Boolean success = true;

        try{
            super.establishConnection(CLRESOURCES);
            success = super.unregisterElement(clusterId);            
            super.disconnect();
            
        } catch (Exception ex) {
            success = false;
        } 
        
        return success;
    }
        
    public String access(String clusterId){
        
        String element = "null";
        try{
            super.establishConnection(CLRESOURCES);
            element = super.accessElement(clusterId);
            super.disconnect();
            
        } catch (Exception ex) {
        	element = "null";
        } 
        
        return element;
    }
}
