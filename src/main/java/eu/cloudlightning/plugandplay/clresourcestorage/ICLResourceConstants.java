/*
 * Set of DB table names (or collections).
 */
package eu.cloudlightning.plugandplay.clresourcestorage;

/**
 *
 * @author Gabriel Gonzalez
 */
 public interface ICLResourceConstants {

    public static final String CLUSTER = "cluster";
    public static final String FRAMEWORK = "framework";
    public static final String TELEMETRY = "telemetry";
    public static final String NODES = "node";
    public static final String MPCXNODES = "mpcxNode";
    public static final String HARDWARE = "hardware";
    public static final String SOFTWARE = "software";
    public static final String CPUS = "cpu";
    public static final String MICS = "mic";
    public static final String GPUS = "gpu";
    public static final String DFES = "dfe";
    public static final String LIBRARIES = "library";
    public static final String APIS = "api";
    public static final String DRIVERS = "driver"; 
    public static final String NETWORKS = "network"; 
    public static final String STORAGES = "storage"; 

}
