
package eu.cloudlightning.plugandplay.resourcemodel;

import java.util.LinkedHashSet;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


/**
 * Plug and Play info
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "head",
    "body"
})
public class ResourceDescription {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("head")
    private Head head;
    @JsonProperty("body")
    @JsonDeserialize(as = java.util.LinkedHashSet.class)
    private Set<Body> body = new LinkedHashSet<Body>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public ResourceDescription() {
    }

    /**
     * 
     * @param body
     * @param head
     */
    public ResourceDescription(Head head, Set<Body> body) {
        super();
        this.head = head;
        this.body = body;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("head")
    public Head getHead() {
        return head;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("head")
    public void setHead(Head head) {
        this.head = head;
    }

    @JsonProperty("body")
    public Set<Body> getBody() {
        return body;
    }

    @JsonProperty("body")
    public void setBody(Set<Body> body) {
        this.body = body;
    }

}
