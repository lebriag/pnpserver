/*
 * This class recover a set of data from the DB to build the registration description
 */
package eu.cloudlightning.plugandplay.resourcemodel;

import eu.cloudlightning.bcri.clresource.CLResource;
import eu.cloudlightning.plugandplay.resourcemodel.CLDBConnector;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author gabriel
 */
public class RecoverRegistrationDescription extends CLDBConnector{

    public RecoverRegistrationDescription(String userIdAuth, String userPwdAuth, String host, String port, String name, String protocol) {
        super(userIdAuth, userPwdAuth, host, port, name, protocol);
    }
        
    public String recoverRegistrationDescription(String clusterId){

        JSONParser parser = new JSONParser();
        Boolean statusOk = true;
        String clResource = "";
        
            super.establishConnection(super.CLRESOURCES);            
            clResource = super.accessElement(clusterId);
            super.disconnect();

        
        return clResource;
    }
    

}
