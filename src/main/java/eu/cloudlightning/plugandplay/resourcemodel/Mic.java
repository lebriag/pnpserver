
package eu.cloudlightning.plugandplay.resourcemodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "model",
    "vendor",
    "product_details",
    "memory",
    "num_cores",
    "frequency_cores",
    "pci_speed"
})
public class Mic {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    private String id;
    @JsonProperty("model")
    private String model;
    @JsonProperty("vendor")
    private String vendor;
    @JsonProperty("product_details")
    private String productDetails;
    /**
     * Calculated in MBs
     * 
     */
    @JsonProperty("memory")
    @JsonPropertyDescription("Calculated in MBs")
    private Double memory;
    @JsonProperty("num_cores")
    private Integer numCores;
    /**
     * Calculated in KHz
     * 
     */
    @JsonProperty("frequency_cores")
    @JsonPropertyDescription("Calculated in KHz")
    private Double frequencyCores;
    /**
     * Calculated in GT/s
     * 
     */
    @JsonProperty("pci_speed")
    @JsonPropertyDescription("Calculated in GT/s")
    private Double pciSpeed;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Mic() {
    }

    /**
     * 
     * @param id
     * @param numCores
     * @param model
     * @param pciSpeed
     * @param vendor
     * @param productDetails
     * @param frequencyCores
     * @param memory
     */
    public Mic(String id, String model, String vendor, String productDetails, Double memory, Integer numCores, Double frequencyCores, Double pciSpeed) {
        super();
        this.id = id;
        this.model = model;
        this.vendor = vendor;
        this.productDetails = productDetails;
        this.memory = memory;
        this.numCores = numCores;
        this.frequencyCores = frequencyCores;
        this.pciSpeed = pciSpeed;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("model")
    public String getModel() {
        return model;
    }

    @JsonProperty("model")
    public void setModel(String model) {
        this.model = model;
    }

    @JsonProperty("vendor")
    public String getVendor() {
        return vendor;
    }

    @JsonProperty("vendor")
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    @JsonProperty("product_details")
    public String getProductDetails() {
        return productDetails;
    }

    @JsonProperty("product_details")
    public void setProductDetails(String productDetails) {
        this.productDetails = productDetails;
    }

    /**
     * Calculated in MBs
     * 
     */
    @JsonProperty("memory")
    public Double getMemory() {
        return memory;
    }

    /**
     * Calculated in MBs
     * 
     */
    @JsonProperty("memory")
    public void setMemory(Double memory) {
        this.memory = memory;
    }

    @JsonProperty("num_cores")
    public Integer getNumCores() {
        return numCores;
    }

    @JsonProperty("num_cores")
    public void setNumCores(Integer numCores) {
        this.numCores = numCores;
    }

    /**
     * Calculated in KHz
     * 
     */
    @JsonProperty("frequency_cores")
    public Double getFrequencyCores() {
        return frequencyCores;
    }

    /**
     * Calculated in KHz
     * 
     */
    @JsonProperty("frequency_cores")
    public void setFrequencyCores(Double frequencyCores) {
        this.frequencyCores = frequencyCores;
    }

    /**
     * Calculated in GT/s
     * 
     */
    @JsonProperty("pci_speed")
    public Double getPciSpeed() {
        return pciSpeed;
    }

    /**
     * Calculated in GT/s
     * 
     */
    @JsonProperty("pci_speed")
    public void setPciSpeed(Double pciSpeed) {
        this.pciSpeed = pciSpeed;
    }

}
