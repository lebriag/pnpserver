
package eu.cloudlightning.plugandplay.resourcemodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "disk_serial",
    "disk_id",
    "type",
    "capacity"
})
public class Storage {

    @JsonProperty("disk_serial")
    private String diskSerial;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("disk_id")
    private String diskId;
    @JsonProperty("type")
    private String type;
    /**
     * Calculated in GB
     * (Required)
     * 
     */
    @JsonProperty("capacity")
    @JsonPropertyDescription("Calculated in GB")
    private Double capacity;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Storage() {
    }

    /**
     * 
     * @param capacity
     * @param diskSerial
     * @param type
     * @param diskId
     */
    public Storage(String diskSerial, String diskId, String type, Double capacity) {
        super();
        this.diskSerial = diskSerial;
        this.diskId = diskId;
        this.type = type;
        this.capacity = capacity;
    }

    @JsonProperty("disk_serial")
    public String getDiskSerial() {
        return diskSerial;
    }

    @JsonProperty("disk_serial")
    public void setDiskSerial(String diskSerial) {
        this.diskSerial = diskSerial;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("disk_id")
    public String getDiskId() {
        return diskId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("disk_id")
    public void setDiskId(String diskId) {
        this.diskId = diskId;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Calculated in GB
     * (Required)
     * 
     */
    @JsonProperty("capacity")
    public Double getCapacity() {
        return capacity;
    }

    /**
     * Calculated in GB
     * (Required)
     * 
     */
    @JsonProperty("capacity")
    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

}
