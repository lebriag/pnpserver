
package eu.cloudlightning.plugandplay.resourcemodel;

import java.util.LinkedHashSet;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cpu",
    "memory",
    "network",
    "storage",
    "co_processors"
})
public class Hardware {

    @JsonProperty("cpu")
    private Cpu cpu;
    @JsonProperty("memory")
    private Memory memory;
    @JsonProperty("network")
    @JsonDeserialize(as = java.util.LinkedHashSet.class)
    private Set<Network> network = new LinkedHashSet<Network>();
    @JsonProperty("storage")
    @JsonDeserialize(as = java.util.LinkedHashSet.class)
    private Set<Storage> storage = new LinkedHashSet<Storage>();
    @JsonProperty("co_processors")
    private CoProcessors coProcessors;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Hardware() {
    }

    /**
     * 
     * @param coProcessors
     * @param cpu
     * @param network
     * @param memory
     * @param storage
     */
    public Hardware(Cpu cpu, Memory memory, Set<Network> network, Set<Storage> storage, CoProcessors coProcessors) {
        super();
        this.cpu = cpu;
        this.memory = memory;
        this.network = network;
        this.storage = storage;
        this.coProcessors = coProcessors;
    }

    @JsonProperty("cpu")
    public Cpu getCpu() {
        return cpu;
    }

    @JsonProperty("cpu")
    public void setCpu(Cpu cpu) {
        this.cpu = cpu;
    }

    @JsonProperty("memory")
    public Memory getMemory() {
        return memory;
    }

    @JsonProperty("memory")
    public void setMemory(Memory memory) {
        this.memory = memory;
    }

    @JsonProperty("network")
    public Set<Network> getNetwork() {
        return network;
    }

    @JsonProperty("network")
    public void setNetwork(Set<Network> network) {
        this.network = network;
    }

    @JsonProperty("storage")
    public Set<Storage> getStorage() {
        return storage;
    }

    @JsonProperty("storage")
    public void setStorage(Set<Storage> storage) {
        this.storage = storage;
    }

    @JsonProperty("co_processors")
    public CoProcessors getCoProcessors() {
        return coProcessors;
    }

    @JsonProperty("co_processors")
    public void setCoProcessors(CoProcessors coProcessors) {
        this.coProcessors = coProcessors;
    }

}
