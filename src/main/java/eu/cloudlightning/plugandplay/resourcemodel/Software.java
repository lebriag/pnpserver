
package eu.cloudlightning.plugandplay.resourcemodel;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "os",
    "libraries",
    "drivers",
    "apis"
})
public class Software {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("os")
    private Os os;
    @JsonProperty("libraries")
    private List<Library> libraries = new ArrayList<Library>();
    @JsonProperty("drivers")
    private List<Driver> drivers = new ArrayList<Driver>();
    @JsonProperty("apis")
    private List<Api> apis = new ArrayList<Api>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Software() {
    }

    /**
     * 
     * @param drivers
     * @param libraries
     * @param os
     * @param apis
     */
    public Software(Os os, List<Library> libraries, List<Driver> drivers, List<Api> apis) {
        super();
        this.os = os;
        this.libraries = libraries;
        this.drivers = drivers;
        this.apis = apis;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("os")
    public Os getOs() {
        return os;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("os")
    public void setOs(Os os) {
        this.os = os;
    }

    @JsonProperty("libraries")
    public List<Library> getLibraries() {
        return libraries;
    }

    @JsonProperty("libraries")
    public void setLibraries(List<Library> libraries) {
        this.libraries = libraries;
    }

    @JsonProperty("drivers")
    public List<Driver> getDrivers() {
        return drivers;
    }

    @JsonProperty("drivers")
    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }

    @JsonProperty("apis")
    public List<Api> getApis() {
        return apis;
    }

    @JsonProperty("apis")
    public void setApis(List<Api> apis) {
        this.apis = apis;
    }

}
