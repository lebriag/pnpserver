
package eu.cloudlightning.plugandplay.resourcemodel;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "mic",
    "gpu",
    "dfe"
})
public class CoProcessors {

    @JsonProperty("mic")
    private List<Mic> mic = new ArrayList<Mic>();
    @JsonProperty("gpu")
    private List<Gpu> gpu = new ArrayList<Gpu>();
    @JsonProperty("dfe")
    private List<Dfe> dfe = new ArrayList<Dfe>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public CoProcessors() {
    }

    /**
     * 
     * @param dfe
     * @param mic
     * @param gpu
     */
    public CoProcessors(List<Mic> mic, List<Gpu> gpu, List<Dfe> dfe) {
        super();
        this.mic = mic;
        this.gpu = gpu;
        this.dfe = dfe;
    }

    @JsonProperty("mic")
    public List<Mic> getMic() {
        return mic;
    }

    @JsonProperty("mic")
    public void setMic(List<Mic> mic) {
        this.mic = mic;
    }

    @JsonProperty("gpu")
    public List<Gpu> getGpu() {
        return gpu;
    }

    @JsonProperty("gpu")
    public void setGpu(List<Gpu> gpu) {
        this.gpu = gpu;
    }

    @JsonProperty("dfe")
    public List<Dfe> getDfe() {
        return dfe;
    }

    @JsonProperty("dfe")
    public void setDfe(List<Dfe> dfe) {
        this.dfe = dfe;
    }

}
