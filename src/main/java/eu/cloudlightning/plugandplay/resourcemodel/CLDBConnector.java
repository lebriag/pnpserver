/*
 * This class has the main methods to register and unregister elements from a DB.
 * In addition it contains a set of operations to get an element/set of elements from 
 * a DB by the different elements _ids that consists of the registrations that are
 * contained into the DB schema
 */
package eu.cloudlightning.plugandplay.resourcemodel;

import eu.cloudlightning.clmongodbconnector.CLMongoDBConnector;
import org.json.simple.JSONObject;

/**
 *
 * @author Gabriel Gonzalez Castane
 */

public class CLDBConnector extends CLMongoDBConnector {
  
    public static String CLRESOURCES = "clresources";

    public CLDBConnector(String userIdAuth, String userPwdAuth, String host, String port, String name, String protocol) {        
        super(userIdAuth, userPwdAuth, host, port, name, protocol);
    }
        
    
    protected Boolean registerElement(JSONObject object,String dbTable){
        Boolean success = true;
        try{           
            // telemetry
            super.changeDbTable(dbTable);
            success = super.recordElement(object);

        }
        catch (Exception p){
            System.out.printf("Error recording " + object + ": to : " + dbTable);
            success = false;
        }
        
        return success;
    }
    
        public String recoverRegistrationDescription(String clusterId, String dbTable){

        	String clResource = "";
        
    	super.changeDbTable(dbTable);
        clResource = (super.accessElement(clusterId));
       
        return clResource;
    }
  
    
    protected Boolean unregisterElement(String id){
        Boolean success = true;
        try{           
            // telemetry
            super.changeDbTable(CLRESOURCES);
            success = super.deleteElement(id);

        }
        catch (Exception p){
            System.out.printf("Error erasing " + id + ": from collection: " + CLRESOURCES);
            success = false;
        }
        
        return success;
    }
 
    
}
