
package eu.cloudlightning.plugandplay.resourcemodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "product_name",
    "gpu_0000",
    "product_brand",
    "persistence_mode",
    "gpu_uuid",
    "minor_number",
    "vbios_version",
    "bus",
    "device",
    "domain",
    "device_id",
    "bus_id",
    "sub_system_id",
    "replays_since_reset",
    "tx_throughput",
    "rx_throughput",
    "memory_total",
    "compute_mode",
    "applications_clock_graphics",
    "applications_clock_memory"
})
public class Gpu {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    private Integer id;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("product_name")
    private String productName;
    @JsonProperty("gpu_0000")
    private Integer gpu0000;
    @JsonProperty("product_brand")
    private String productBrand;
    @JsonProperty("persistence_mode")
    private Integer persistenceMode;
    @JsonProperty("gpu_uuid")
    private String gpuUuid;
    @JsonProperty("minor_number")
    private Integer minorNumber;
    @JsonProperty("vbios_version")
    private String vbiosVersion;
    @JsonProperty("bus")
    private Integer bus;
    @JsonProperty("device")
    private Integer device;
    @JsonProperty("domain")
    private Integer domain;
    @JsonProperty("device_id")
    private Integer deviceId;
    @JsonProperty("bus_id")
    private String busId;
    @JsonProperty("sub_system_id")
    private Integer subSystemId;
    @JsonProperty("replays_since_reset")
    private Integer replaysSinceReset;
    /**
     * Calculated in KB/s
     * 
     */
    @JsonProperty("tx_throughput")
    @JsonPropertyDescription("Calculated in KB/s")
    private Double txThroughput;
    /**
     * Calculated in KB/s
     * 
     */
    @JsonProperty("rx_throughput")
    @JsonPropertyDescription("Calculated in KB/s")
    private Double rxThroughput;
    /**
     * Calculated in MiB
     * (Required)
     * 
     */
    @JsonProperty("memory_total")
    @JsonPropertyDescription("Calculated in MiB")
    private Double memoryTotal;
    @JsonProperty("compute_mode")
    private Integer computeMode;
    /**
     * Calculated in MHz
     * 
     */
    @JsonProperty("applications_clock_graphics")
    @JsonPropertyDescription("Calculated in MHz")
    private Double applicationsClockGraphics;
    /**
     * Calculated in MHz
     * 
     */
    @JsonProperty("applications_clock_memory")
    @JsonPropertyDescription("Calculated in MHz")
    private Double applicationsClockMemory;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Gpu() {
    }

    /**
     * 
     * @param vbiosVersion
     * @param bus
     * @param busId
     * @param subSystemId
     * @param replaysSinceReset
     * @param minorNumber
     * @param memoryTotal
     * @param persistenceMode
     * @param gpuUuid
     * @param id
     * @param gpu0000
     * @param applicationsClockGraphics
     * @param rxThroughput
     * @param productBrand
     * @param applicationsClockMemory
     * @param txThroughput
     * @param device
     * @param domain
     * @param computeMode
     * @param productName
     * @param deviceId
     */
    public Gpu(Integer id, String productName, Integer gpu0000, String productBrand, Integer persistenceMode, String gpuUuid, Integer minorNumber, String vbiosVersion, Integer bus, Integer device, Integer domain, Integer deviceId, String busId, Integer subSystemId, Integer replaysSinceReset, Double txThroughput, Double rxThroughput, Double memoryTotal, Integer computeMode, Double applicationsClockGraphics, Double applicationsClockMemory) {
        super();
        this.id = id;
        this.productName = productName;
        this.gpu0000 = gpu0000;
        this.productBrand = productBrand;
        this.persistenceMode = persistenceMode;
        this.gpuUuid = gpuUuid;
        this.minorNumber = minorNumber;
        this.vbiosVersion = vbiosVersion;
        this.bus = bus;
        this.device = device;
        this.domain = domain;
        this.deviceId = deviceId;
        this.busId = busId;
        this.subSystemId = subSystemId;
        this.replaysSinceReset = replaysSinceReset;
        this.txThroughput = txThroughput;
        this.rxThroughput = rxThroughput;
        this.memoryTotal = memoryTotal;
        this.computeMode = computeMode;
        this.applicationsClockGraphics = applicationsClockGraphics;
        this.applicationsClockMemory = applicationsClockMemory;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("product_name")
    public String getProductName() {
        return productName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("product_name")
    public void setProductName(String productName) {
        this.productName = productName;
    }

    @JsonProperty("gpu_0000")
    public Integer getGpu0000() {
        return gpu0000;
    }

    @JsonProperty("gpu_0000")
    public void setGpu0000(Integer gpu0000) {
        this.gpu0000 = gpu0000;
    }

    @JsonProperty("product_brand")
    public String getProductBrand() {
        return productBrand;
    }

    @JsonProperty("product_brand")
    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    @JsonProperty("persistence_mode")
    public Integer getPersistenceMode() {
        return persistenceMode;
    }

    @JsonProperty("persistence_mode")
    public void setPersistenceMode(Integer persistenceMode) {
        this.persistenceMode = persistenceMode;
    }

    @JsonProperty("gpu_uuid")
    public String getGpuUuid() {
        return gpuUuid;
    }

    @JsonProperty("gpu_uuid")
    public void setGpuUuid(String gpuUuid) {
        this.gpuUuid = gpuUuid;
    }

    @JsonProperty("minor_number")
    public Integer getMinorNumber() {
        return minorNumber;
    }

    @JsonProperty("minor_number")
    public void setMinorNumber(Integer minorNumber) {
        this.minorNumber = minorNumber;
    }

    @JsonProperty("vbios_version")
    public String getVbiosVersion() {
        return vbiosVersion;
    }

    @JsonProperty("vbios_version")
    public void setVbiosVersion(String vbiosVersion) {
        this.vbiosVersion = vbiosVersion;
    }

    @JsonProperty("bus")
    public Integer getBus() {
        return bus;
    }

    @JsonProperty("bus")
    public void setBus(Integer bus) {
        this.bus = bus;
    }

    @JsonProperty("device")
    public Integer getDevice() {
        return device;
    }

    @JsonProperty("device")
    public void setDevice(Integer device) {
        this.device = device;
    }

    @JsonProperty("domain")
    public Integer getDomain() {
        return domain;
    }

    @JsonProperty("domain")
    public void setDomain(Integer domain) {
        this.domain = domain;
    }

    @JsonProperty("device_id")
    public Integer getDeviceId() {
        return deviceId;
    }

    @JsonProperty("device_id")
    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    @JsonProperty("bus_id")
    public String getBusId() {
        return busId;
    }

    @JsonProperty("bus_id")
    public void setBusId(String busId) {
        this.busId = busId;
    }

    @JsonProperty("sub_system_id")
    public Integer getSubSystemId() {
        return subSystemId;
    }

    @JsonProperty("sub_system_id")
    public void setSubSystemId(Integer subSystemId) {
        this.subSystemId = subSystemId;
    }

    @JsonProperty("replays_since_reset")
    public Integer getReplaysSinceReset() {
        return replaysSinceReset;
    }

    @JsonProperty("replays_since_reset")
    public void setReplaysSinceReset(Integer replaysSinceReset) {
        this.replaysSinceReset = replaysSinceReset;
    }

    /**
     * Calculated in KB/s
     * 
     */
    @JsonProperty("tx_throughput")
    public Double getTxThroughput() {
        return txThroughput;
    }

    /**
     * Calculated in KB/s
     * 
     */
    @JsonProperty("tx_throughput")
    public void setTxThroughput(Double txThroughput) {
        this.txThroughput = txThroughput;
    }

    /**
     * Calculated in KB/s
     * 
     */
    @JsonProperty("rx_throughput")
    public Double getRxThroughput() {
        return rxThroughput;
    }

    /**
     * Calculated in KB/s
     * 
     */
    @JsonProperty("rx_throughput")
    public void setRxThroughput(Double rxThroughput) {
        this.rxThroughput = rxThroughput;
    }

    /**
     * Calculated in MiB
     * (Required)
     * 
     */
    @JsonProperty("memory_total")
    public Double getMemoryTotal() {
        return memoryTotal;
    }

    /**
     * Calculated in MiB
     * (Required)
     * 
     */
    @JsonProperty("memory_total")
    public void setMemoryTotal(Double memoryTotal) {
        this.memoryTotal = memoryTotal;
    }

    @JsonProperty("compute_mode")
    public Integer getComputeMode() {
        return computeMode;
    }

    @JsonProperty("compute_mode")
    public void setComputeMode(Integer computeMode) {
        this.computeMode = computeMode;
    }

    /**
     * Calculated in MHz
     * 
     */
    @JsonProperty("applications_clock_graphics")
    public Double getApplicationsClockGraphics() {
        return applicationsClockGraphics;
    }

    /**
     * Calculated in MHz
     * 
     */
    @JsonProperty("applications_clock_graphics")
    public void setApplicationsClockGraphics(Double applicationsClockGraphics) {
        this.applicationsClockGraphics = applicationsClockGraphics;
    }

    /**
     * Calculated in MHz
     * 
     */
    @JsonProperty("applications_clock_memory")
    public Double getApplicationsClockMemory() {
        return applicationsClockMemory;
    }

    /**
     * Calculated in MHz
     * 
     */
    @JsonProperty("applications_clock_memory")
    public void setApplicationsClockMemory(Double applicationsClockMemory) {
        this.applicationsClockMemory = applicationsClockMemory;
    }

}
