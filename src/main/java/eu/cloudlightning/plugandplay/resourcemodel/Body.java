
package eu.cloudlightning.plugandplay.resourcemodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "node_id",
    "node_ip",
    "host_name",
    "accessing_framework",
    "hardware",
    "software"
})
public class Body {

    @JsonProperty("node_id")
    private String nodeId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("node_ip")
    private String nodeIp;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("host_name")
    private String hostName;
    @JsonProperty("accessing_framework")
    private String accessingFramework;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("hardware")
    private Hardware hardware;
    @JsonProperty("software")
    private Software software;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Body() {
    }

    /**
     * 
     * @param accessingFramework
     * @param nodeIp
     * @param software
     * @param nodeId
     * @param hardware
     * @param hostName
     */
    public Body(String nodeId, String nodeIp, String hostName, String accessingFramework, Hardware hardware, Software software) {
        super();
        this.nodeId = nodeId;
        this.nodeIp = nodeIp;
        this.hostName = hostName;
        this.accessingFramework = accessingFramework;
        this.hardware = hardware;
        this.software = software;
    }

    @JsonProperty("node_id")
    public String getNodeId() {
        return nodeId;
    }

    @JsonProperty("node_id")
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("node_ip")
    public String getNodeIp() {
        return nodeIp;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("node_ip")
    public void setNodeIp(String nodeIp) {
        this.nodeIp = nodeIp;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("host_name")
    public String getHostName() {
        return hostName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("host_name")
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    @JsonProperty("accessing_framework")
    public String getAccessingFramework() {
        return accessingFramework;
    }

    @JsonProperty("accessing_framework")
    public void setAccessingFramework(String accessingFramework) {
        this.accessingFramework = accessingFramework;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("hardware")
    public Hardware getHardware() {
        return hardware;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("hardware")
    public void setHardware(Hardware hardware) {
        this.hardware = hardware;
    }

    @JsonProperty("software")
    public Software getSoftware() {
        return software;
    }

    @JsonProperty("software")
    public void setSoftware(Software software) {
        this.software = software;
    }

}
