
package eu.cloudlightning.plugandplay.resourcemodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "architecture",
    "cpu_op_modes",
    "byte_order",
    "cpus",
    "on_line_cpu_list",
    "thread_per_core",
    "cores_per_socket",
    "sockets",
    "numa_node",
    "vendor_id",
    "cpu_family",
    "model",
    "model_name",
    "stepping",
    "cpu_frequency",
    "bogomips",
    "virtualization",
    "l1d_cache",
    "l1i_cache",
    "l2_cache",
    "l3_cache",
    "flags"
})
public class Cpu {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("architecture")
    private String architecture;
    @JsonProperty("cpu_op_modes")
    private String cpuOpModes;
    @JsonProperty("byte_order")
    private String byteOrder;
    @JsonProperty("cpus")
    private Integer cpus;
    @JsonProperty("on_line_cpu_list")
    private String onLineCpuList;
    @JsonProperty("thread_per_core")
    private Integer threadPerCore;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cores_per_socket")
    private Integer coresPerSocket;
    @JsonProperty("sockets")
    private Integer sockets;
    @JsonProperty("numa_node")
    private Integer numaNode;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("vendor_id")
    private String vendorId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cpu_family")
    private Integer cpuFamily;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("model")
    private Integer model;
    @JsonProperty("model_name")
    private String modelName;
    @JsonProperty("stepping")
    private Integer stepping;
    /**
     * Calculated in MHz
     * 
     */
    @JsonProperty("cpu_frequency")
    @JsonPropertyDescription("Calculated in MHz")
    private Double cpuFrequency;
    /**
     * Calculated in MHz
     * 
     */
    @JsonProperty("bogomips")
    @JsonPropertyDescription("Calculated in MHz")
    private Double bogomips;
    @JsonProperty("virtualization")
    private String virtualization;
    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("l1d_cache")
    @JsonPropertyDescription("Calculated in KB")
    private Double l1dCache;
    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("l1i_cache")
    @JsonPropertyDescription("Calculated in KB")
    private Double l1iCache;
    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("l2_cache")
    @JsonPropertyDescription("Calculated in KB")
    private Double l2Cache;
    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("l3_cache")
    @JsonPropertyDescription("Calculated in KB")
    private Double l3Cache;
    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("flags")
    @JsonPropertyDescription("Calculated in KB")
    private String flags;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Cpu() {
    }

    /**
     * 
     * @param numaNode
     * @param byteOrder
     * @param model
     * @param flags
     * @param bogomips
     * @param stepping
     * @param virtualization
     * @param l1iCache
     * @param l2Cache
     * @param architecture
     * @param vendorId
     * @param threadPerCore
     * @param sockets
     * @param l3Cache
     * @param cpus
     * @param l1dCache
     * @param modelName
     * @param cpuFrequency
     * @param onLineCpuList
     * @param cpuFamily
     * @param cpuOpModes
     * @param coresPerSocket
     */
    public Cpu(String architecture, String cpuOpModes, String byteOrder, Integer cpus, String onLineCpuList, Integer threadPerCore, Integer coresPerSocket, Integer sockets, Integer numaNode, String vendorId, Integer cpuFamily, Integer model, String modelName, Integer stepping, Double cpuFrequency, Double bogomips, String virtualization, Double l1dCache, Double l1iCache, Double l2Cache, Double l3Cache, String flags) {
        super();
        this.architecture = architecture;
        this.cpuOpModes = cpuOpModes;
        this.byteOrder = byteOrder;
        this.cpus = cpus;
        this.onLineCpuList = onLineCpuList;
        this.threadPerCore = threadPerCore;
        this.coresPerSocket = coresPerSocket;
        this.sockets = sockets;
        this.numaNode = numaNode;
        this.vendorId = vendorId;
        this.cpuFamily = cpuFamily;
        this.model = model;
        this.modelName = modelName;
        this.stepping = stepping;
        this.cpuFrequency = cpuFrequency;
        this.bogomips = bogomips;
        this.virtualization = virtualization;
        this.l1dCache = l1dCache;
        this.l1iCache = l1iCache;
        this.l2Cache = l2Cache;
        this.l3Cache = l3Cache;
        this.flags = flags;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("architecture")
    public String getArchitecture() {
        return architecture;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("architecture")
    public void setArchitecture(String architecture) {
        this.architecture = architecture;
    }

    @JsonProperty("cpu_op_modes")
    public String getCpuOpModes() {
        return cpuOpModes;
    }

    @JsonProperty("cpu_op_modes")
    public void setCpuOpModes(String cpuOpModes) {
        this.cpuOpModes = cpuOpModes;
    }

    @JsonProperty("byte_order")
    public String getByteOrder() {
        return byteOrder;
    }

    @JsonProperty("byte_order")
    public void setByteOrder(String byteOrder) {
        this.byteOrder = byteOrder;
    }

    @JsonProperty("cpus")
    public Integer getCpus() {
        return cpus;
    }

    @JsonProperty("cpus")
    public void setCpus(Integer cpus) {
        this.cpus = cpus;
    }

    @JsonProperty("on_line_cpu_list")
    public String getOnLineCpuList() {
        return onLineCpuList;
    }

    @JsonProperty("on_line_cpu_list")
    public void setOnLineCpuList(String onLineCpuList) {
        this.onLineCpuList = onLineCpuList;
    }

    @JsonProperty("thread_per_core")
    public Integer getThreadPerCore() {
        return threadPerCore;
    }

    @JsonProperty("thread_per_core")
    public void setThreadPerCore(Integer threadPerCore) {
        this.threadPerCore = threadPerCore;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cores_per_socket")
    public Integer getCoresPerSocket() {
        return coresPerSocket;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cores_per_socket")
    public void setCoresPerSocket(Integer coresPerSocket) {
        this.coresPerSocket = coresPerSocket;
    }

    @JsonProperty("sockets")
    public Integer getSockets() {
        return sockets;
    }

    @JsonProperty("sockets")
    public void setSockets(Integer sockets) {
        this.sockets = sockets;
    }

    @JsonProperty("numa_node")
    public Integer getNumaNode() {
        return numaNode;
    }

    @JsonProperty("numa_node")
    public void setNumaNode(Integer numaNode) {
        this.numaNode = numaNode;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("vendor_id")
    public String getVendorId() {
        return vendorId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("vendor_id")
    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cpu_family")
    public Integer getCpuFamily() {
        return cpuFamily;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("cpu_family")
    public void setCpuFamily(Integer cpuFamily) {
        this.cpuFamily = cpuFamily;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("model")
    public Integer getModel() {
        return model;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("model")
    public void setModel(Integer model) {
        this.model = model;
    }

    @JsonProperty("model_name")
    public String getModelName() {
        return modelName;
    }

    @JsonProperty("model_name")
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    @JsonProperty("stepping")
    public Integer getStepping() {
        return stepping;
    }

    @JsonProperty("stepping")
    public void setStepping(Integer stepping) {
        this.stepping = stepping;
    }

    /**
     * Calculated in MHz
     * 
     */
    @JsonProperty("cpu_frequency")
    public Double getCpuFrequency() {
        return cpuFrequency;
    }

    /**
     * Calculated in MHz
     * 
     */
    @JsonProperty("cpu_frequency")
    public void setCpuFrequency(Double cpuFrequency) {
        this.cpuFrequency = cpuFrequency;
    }

    /**
     * Calculated in MHz
     * 
     */
    @JsonProperty("bogomips")
    public Double getBogomips() {
        return bogomips;
    }

    /**
     * Calculated in MHz
     * 
     */
    @JsonProperty("bogomips")
    public void setBogomips(Double bogomips) {
        this.bogomips = bogomips;
    }

    @JsonProperty("virtualization")
    public String getVirtualization() {
        return virtualization;
    }

    @JsonProperty("virtualization")
    public void setVirtualization(String virtualization) {
        this.virtualization = virtualization;
    }

    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("l1d_cache")
    public Double getL1dCache() {
        return l1dCache;
    }

    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("l1d_cache")
    public void setL1dCache(Double l1dCache) {
        this.l1dCache = l1dCache;
    }

    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("l1i_cache")
    public Double getL1iCache() {
        return l1iCache;
    }

    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("l1i_cache")
    public void setL1iCache(Double l1iCache) {
        this.l1iCache = l1iCache;
    }

    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("l2_cache")
    public Double getL2Cache() {
        return l2Cache;
    }

    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("l2_cache")
    public void setL2Cache(Double l2Cache) {
        this.l2Cache = l2Cache;
    }

    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("l3_cache")
    public Double getL3Cache() {
        return l3Cache;
    }

    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("l3_cache")
    public void setL3Cache(Double l3Cache) {
        this.l3Cache = l3Cache;
    }

    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("flags")
    public String getFlags() {
        return flags;
    }

    /**
     * Calculated in KB
     * 
     */
    @JsonProperty("flags")
    public void setFlags(String flags) {
        this.flags = flags;
    }

}
