
package eu.cloudlightning.plugandplay.resourcemodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ram_capacity",
    "ram_type"
})
public class Memory {

    /**
     * Calculated in MB
     * (Required)
     * 
     */
    @JsonProperty("ram_capacity")
    @JsonPropertyDescription("Calculated in MB")
    private Double ramCapacity;
    @JsonProperty("ram_type")
    private String ramType;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Memory() {
    }

    /**
     * 
     * @param ramCapacity
     * @param ramType
     */
    public Memory(Double ramCapacity, String ramType) {
        super();
        this.ramCapacity = ramCapacity;
        this.ramType = ramType;
    }

    /**
     * Calculated in MB
     * (Required)
     * 
     */
    @JsonProperty("ram_capacity")
    public Double getRamCapacity() {
        return ramCapacity;
    }

    /**
     * Calculated in MB
     * (Required)
     * 
     */
    @JsonProperty("ram_capacity")
    public void setRamCapacity(Double ramCapacity) {
        this.ramCapacity = ramCapacity;
    }

    @JsonProperty("ram_type")
    public String getRamType() {
        return ramType;
    }

    @JsonProperty("ram_type")
    public void setRamType(String ramType) {
        this.ramType = ramType;
    }

}
