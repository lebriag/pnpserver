
package eu.cloudlightning.plugandplay.resourcemodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "os_type",
    "kernel_version",
    "os_distribution",
    "distribution_release"
})
public class Os {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("os_type")
    private String osType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("kernel_version")
    private String kernelVersion;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("os_distribution")
    private String osDistribution;
    @JsonProperty("distribution_release")
    private String distributionRelease;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Os() {
    }

    /**
     * 
     * @param osDistribution
     * @param distributionRelease
     * @param kernelVersion
     * @param osType
     */
    public Os(String osType, String kernelVersion, String osDistribution, String distributionRelease) {
        super();
        this.osType = osType;
        this.kernelVersion = kernelVersion;
        this.osDistribution = osDistribution;
        this.distributionRelease = distributionRelease;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("os_type")
    public String getOsType() {
        return osType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("os_type")
    public void setOsType(String osType) {
        this.osType = osType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("kernel_version")
    public String getKernelVersion() {
        return kernelVersion;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("kernel_version")
    public void setKernelVersion(String kernelVersion) {
        this.kernelVersion = kernelVersion;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("os_distribution")
    public String getOsDistribution() {
        return osDistribution;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("os_distribution")
    public void setOsDistribution(String osDistribution) {
        this.osDistribution = osDistribution;
    }

    @JsonProperty("distribution_release")
    public String getDistributionRelease() {
        return distributionRelease;
    }

    @JsonProperty("distribution_release")
    public void setDistributionRelease(String distributionRelease) {
        this.distributionRelease = distributionRelease;
    }

}
