
package eu.cloudlightning.plugandplay.resourcemodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

//@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "deployment_url",
    "username",
    "password",
    "accesskey",
    "userdomainname",
    "projectid"
})
public class Deployment {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    private String name;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("deployment_url")
    private String deploymentUrl;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("username")
    private String username;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("password")
    private String password;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("accesskey")
    private String accesskey;

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("userdomainname")
    private String userdomainname;
    
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("projectid")
    private String projectid;
    /**
     * No args constructor for use in serialization
     * 
     */
    
    public Deployment() {
    }

    /**
     * 
     * @param username
     * @param name
     * @param password
     * @param deploymentUrl
     * @param accesskey
     */
    public Deployment(String name, String deploymentUrl, String username, String password, String accesskey) {
        super();
        this.name = name;
        this.deploymentUrl = deploymentUrl;
        this.username = username;
        this.password = password;
        this.accesskey = accesskey;
    }

    public Deployment(String name, String deploymentUrl, String username, String password, String accesskey, String userdomainname, String projectid) {
        super();
        this.name = name;
        this.deploymentUrl = deploymentUrl;
        this.username = username;
        this.password = password;
        this.accesskey = accesskey;
        this.userdomainname = userdomainname;
        this.projectid = projectid;
    }
    
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("deployment_url")
    public String getDeploymentUrl() {
        return deploymentUrl;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("deployment_url")
    public void setDeploymentUrl(String deploymentUrl) {
        this.deploymentUrl = deploymentUrl;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("accesskey")
    public String getAccesskey() {
        return accesskey;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("accesskey")
    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }
    
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("userdomainname")
    public String getUserdomainname() {
        return userdomainname;
    }

    /**
     * 
     * (Optional)
     * 
     */
    @JsonProperty("userdomainname")
    public void setUserdomainname(String userdomainname) {
        this.userdomainname = userdomainname;
    }
    
    /**
     * 
     * (Optional)
     * 
     */
    @JsonProperty("projectid")
    public String getProjectid() {
        return projectid;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("projectid")
    public void setProjectid(String projectid) {
        this.projectid = projectid;
    }
    
}
