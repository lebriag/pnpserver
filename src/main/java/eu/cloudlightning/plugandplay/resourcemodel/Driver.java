
package eu.cloudlightning.plugandplay.resourcemodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "version",
    "kernel_version"
})
public class Driver {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    private String name;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("version")
    private String version;
    @JsonProperty("kernel_version")
    private String kernelVersion;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Driver() {
    }

    /**
     * 
     * @param name
     * @param kernelVersion
     * @param version
     */
    public Driver(String name, String version, String kernelVersion) {
        super();
        this.name = name;
        this.version = version;
        this.kernelVersion = kernelVersion;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("kernel_version")
    public String getKernelVersion() {
        return kernelVersion;
    }

    @JsonProperty("kernel_version")
    public void setKernelVersion(String kernelVersion) {
        this.kernelVersion = kernelVersion;
    }

}
