
package eu.cloudlightning.plugandplay.resourcemodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "type",
    "speed",
    "interface",
    "endpoint"
})
public class Network {

    @JsonProperty("id")
    private String id;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    private String type;
    /**
     * Calculated in Mbit/s
     * (Required)
     * 
     */
    @JsonProperty("speed")
    @JsonPropertyDescription("Calculated in Mbit/s")
    private Double speed;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("interface")
    private String _interface;
    @JsonProperty("endpoint")
    private String endpoint;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Network() {
    }

    /**
     * 
     * @param id
     * @param speed
     * @param _interface
     * @param type
     * @param endpoint
     */
    public Network(String id, String type, Double speed, String _interface, String endpoint) {
        super();
        this.id = id;
        this.type = type;
        this.speed = speed;
        this._interface = _interface;
        this.endpoint = endpoint;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Calculated in Mbit/s
     * (Required)
     * 
     */
    @JsonProperty("speed")
    public Double getSpeed() {
        return speed;
    }

    /**
     * Calculated in Mbit/s
     * (Required)
     * 
     */
    @JsonProperty("speed")
    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("interface")
    public String getInterface() {
        return _interface;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("interface")
    public void setInterface(String _interface) {
        this._interface = _interface;
    }

    @JsonProperty("endpoint")
    public String getEndpoint() {
        return endpoint;
    }

    @JsonProperty("endpoint")
    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

}
