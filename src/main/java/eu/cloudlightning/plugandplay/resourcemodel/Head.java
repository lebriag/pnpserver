
package eu.cloudlightning.plugandplay.resourcemodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "manager",
    "telemetry",
    "deployment"
})
public class Head {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("manager")
    private Manager manager;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("telemetry")
    private Telemetry telemetry;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("deployment")
    private Deployment deployment;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Head() {
    }

    /**
     * 
     * @param manager
     * @param telemetry
     * @param deployment
     */
    public Head(Manager manager, Telemetry telemetry, Deployment deployment) {
        super();
        this.manager = manager;
        this.telemetry = telemetry;
        this.deployment = deployment;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("manager")
    public Manager getManager() {
        return manager;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("manager")
    public void setManager(Manager manager) {
        this.manager = manager;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("telemetry")
    public Telemetry getTelemetry() {
        return telemetry;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("telemetry")
    public void setTelemetry(Telemetry telemetry) {
        this.telemetry = telemetry;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("deployment")
    public Deployment getDeployment() {
        return deployment;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("deployment")
    public void setDeployment(Deployment deployment) {
        this.deployment = deployment;
    }

}
