
package eu.cloudlightning.plugandplay.resourcemodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "maxring_connected",
    "maxring_connection_top",
    "maxring_connection_bottom",
    "maxring_bandwidth",
    "generation",
    "model",
    "revision",
    "serial_number",
    "part_number",
    "device_capabilities",
    "fpga_model",
    "fpga_vendor",
    "cpld_version",
    "pcie_generation",
    "pci_lanes",
    "pcie_throughput",
    "lmem_type",
    "lmem_capacity",
    "lmem_bandwidth",
    "lmem_max_clock_rate",
    "lmem_dimms",
    "fmem_type",
    "fmem_capacity",
    "fmem_bandwidth"
})
public class Dfe {

    /**
     * Local-device unique number of a DFE card
     * (Required)
     * 
     */
    @JsonProperty("id")
    @JsonPropertyDescription("Local-device unique number of a DFE card")
    private String id;
    @JsonProperty("maxring_connected")
    private Boolean maxringConnected;
    @JsonProperty("maxring_connection_top")
    private String maxringConnectionTop;
    @JsonProperty("maxring_connection_bottom")
    private String maxringConnectionBottom;
    /**
     * in GB/s
     * 
     */
    @JsonProperty("maxring_bandwidth")
    @JsonPropertyDescription("in GB/s")
    private String maxringBandwidth;
    @JsonProperty("generation")
    private String generation;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("model")
    private String model;
    @JsonProperty("revision")
    private String revision;
    @JsonProperty("serial_number")
    private String serialNumber;
    @JsonProperty("part_number")
    private String partNumber;
    @JsonProperty("device_capabilities")
    private String deviceCapabilities;
    @JsonProperty("fpga_model")
    private String fpgaModel;
    @JsonProperty("fpga_vendor")
    private String fpgaVendor;
    @JsonProperty("cpld_version")
    private String cpldVersion;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("pcie_generation")
    private String pcieGeneration;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("pci_lanes")
    private Integer pciLanes;
    /**
     * In GB/s
     * (Required)
     * 
     */
    @JsonProperty("pcie_throughput")
    @JsonPropertyDescription("In GB/s")
    private Double pcieThroughput;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("lmem_type")
    private String lmemType;
    /**
     * In GB
     * (Required)
     * 
     */
    @JsonProperty("lmem_capacity")
    @JsonPropertyDescription("In GB")
    private Double lmemCapacity;
    /**
     * In GB/s
     * (Required)
     * 
     */
    @JsonProperty("lmem_bandwidth")
    @JsonPropertyDescription("In GB/s")
    private Double lmemBandwidth;
    @JsonProperty("lmem_max_clock_rate")
    private Double lmemMaxClockRate;
    @JsonProperty("lmem_dimms")
    private Double lmemDimms;
    @JsonProperty("fmem_type")
    private String fmemType;
    /**
     * In MB
     * 
     */
    @JsonProperty("fmem_capacity")
    @JsonPropertyDescription("In MB")
    private Double fmemCapacity;
    /**
     * In TB/s
     * 
     */
    @JsonProperty("fmem_bandwidth")
    @JsonPropertyDescription("In TB/s")
    private Double fmemBandwidth;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Dfe() {
    }

    /**
     * 
     * @param fmemBandwidth
     * @param model
     * @param maxringConnectionBottom
     * @param deviceCapabilities
     * @param maxringConnected
     * @param pcieThroughput
     * @param maxringBandwidth
     * @param serialNumber
     * @param lmemMaxClockRate
     * @param id
     * @param cpldVersion
     * @param revision
     * @param pcieGeneration
     * @param fmemType
     * @param pciLanes
     * @param maxringConnectionTop
     * @param fpgaVendor
     * @param fpgaModel
     * @param lmemDimms
     * @param lmemType
     * @param generation
     * @param fmemCapacity
     * @param lmemBandwidth
     * @param lmemCapacity
     * @param partNumber
     */
    public Dfe(String id, Boolean maxringConnected, String maxringConnectionTop, String maxringConnectionBottom, String maxringBandwidth, String generation, String model, String revision, String serialNumber, String partNumber, String deviceCapabilities, String fpgaModel, String fpgaVendor, String cpldVersion, String pcieGeneration, Integer pciLanes, Double pcieThroughput, String lmemType, Double lmemCapacity, Double lmemBandwidth, Double lmemMaxClockRate, Double lmemDimms, String fmemType, Double fmemCapacity, Double fmemBandwidth) {
        super();
        this.id = id;
        this.maxringConnected = maxringConnected;
        this.maxringConnectionTop = maxringConnectionTop;
        this.maxringConnectionBottom = maxringConnectionBottom;
        this.maxringBandwidth = maxringBandwidth;
        this.generation = generation;
        this.model = model;
        this.revision = revision;
        this.serialNumber = serialNumber;
        this.partNumber = partNumber;
        this.deviceCapabilities = deviceCapabilities;
        this.fpgaModel = fpgaModel;
        this.fpgaVendor = fpgaVendor;
        this.cpldVersion = cpldVersion;
        this.pcieGeneration = pcieGeneration;
        this.pciLanes = pciLanes;
        this.pcieThroughput = pcieThroughput;
        this.lmemType = lmemType;
        this.lmemCapacity = lmemCapacity;
        this.lmemBandwidth = lmemBandwidth;
        this.lmemMaxClockRate = lmemMaxClockRate;
        this.lmemDimms = lmemDimms;
        this.fmemType = fmemType;
        this.fmemCapacity = fmemCapacity;
        this.fmemBandwidth = fmemBandwidth;
    }

    /**
     * Local-device unique number of a DFE card
     * (Required)
     * 
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * Local-device unique number of a DFE card
     * (Required)
     * 
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("maxring_connected")
    public Boolean getMaxringConnected() {
        return maxringConnected;
    }

    @JsonProperty("maxring_connected")
    public void setMaxringConnected(Boolean maxringConnected) {
        this.maxringConnected = maxringConnected;
    }

    @JsonProperty("maxring_connection_top")
    public String getMaxringConnectionTop() {
        return maxringConnectionTop;
    }

    @JsonProperty("maxring_connection_top")
    public void setMaxringConnectionTop(String maxringConnectionTop) {
        this.maxringConnectionTop = maxringConnectionTop;
    }

    @JsonProperty("maxring_connection_bottom")
    public String getMaxringConnectionBottom() {
        return maxringConnectionBottom;
    }

    @JsonProperty("maxring_connection_bottom")
    public void setMaxringConnectionBottom(String maxringConnectionBottom) {
        this.maxringConnectionBottom = maxringConnectionBottom;
    }

    /**
     * in GB/s
     * 
     */
    @JsonProperty("maxring_bandwidth")
    public String getMaxringBandwidth() {
        return maxringBandwidth;
    }

    /**
     * in GB/s
     * 
     */
    @JsonProperty("maxring_bandwidth")
    public void setMaxringBandwidth(String maxringBandwidth) {
        this.maxringBandwidth = maxringBandwidth;
    }

    @JsonProperty("generation")
    public String getGeneration() {
        return generation;
    }

    @JsonProperty("generation")
    public void setGeneration(String generation) {
        this.generation = generation;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("model")
    public String getModel() {
        return model;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("model")
    public void setModel(String model) {
        this.model = model;
    }

    @JsonProperty("revision")
    public String getRevision() {
        return revision;
    }

    @JsonProperty("revision")
    public void setRevision(String revision) {
        this.revision = revision;
    }

    @JsonProperty("serial_number")
    public String getSerialNumber() {
        return serialNumber;
    }

    @JsonProperty("serial_number")
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @JsonProperty("part_number")
    public String getPartNumber() {
        return partNumber;
    }

    @JsonProperty("part_number")
    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    @JsonProperty("device_capabilities")
    public String getDeviceCapabilities() {
        return deviceCapabilities;
    }

    @JsonProperty("device_capabilities")
    public void setDeviceCapabilities(String deviceCapabilities) {
        this.deviceCapabilities = deviceCapabilities;
    }

    @JsonProperty("fpga_model")
    public String getFpgaModel() {
        return fpgaModel;
    }

    @JsonProperty("fpga_model")
    public void setFpgaModel(String fpgaModel) {
        this.fpgaModel = fpgaModel;
    }

    @JsonProperty("fpga_vendor")
    public String getFpgaVendor() {
        return fpgaVendor;
    }

    @JsonProperty("fpga_vendor")
    public void setFpgaVendor(String fpgaVendor) {
        this.fpgaVendor = fpgaVendor;
    }

    @JsonProperty("cpld_version")
    public String getCpldVersion() {
        return cpldVersion;
    }

    @JsonProperty("cpld_version")
    public void setCpldVersion(String cpldVersion) {
        this.cpldVersion = cpldVersion;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("pcie_generation")
    public String getPcieGeneration() {
        return pcieGeneration;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("pcie_generation")
    public void setPcieGeneration(String pcieGeneration) {
        this.pcieGeneration = pcieGeneration;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("pci_lanes")
    public Integer getPciLanes() {
        return pciLanes;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("pci_lanes")
    public void setPciLanes(Integer pciLanes) {
        this.pciLanes = pciLanes;
    }

    /**
     * In GB/s
     * (Required)
     * 
     */
    @JsonProperty("pcie_throughput")
    public Double getPcieThroughput() {
        return pcieThroughput;
    }

    /**
     * In GB/s
     * (Required)
     * 
     */
    @JsonProperty("pcie_throughput")
    public void setPcieThroughput(Double pcieThroughput) {
        this.pcieThroughput = pcieThroughput;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("lmem_type")
    public String getLmemType() {
        return lmemType;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("lmem_type")
    public void setLmemType(String lmemType) {
        this.lmemType = lmemType;
    }

    /**
     * In GB
     * (Required)
     * 
     */
    @JsonProperty("lmem_capacity")
    public Double getLmemCapacity() {
        return lmemCapacity;
    }

    /**
     * In GB
     * (Required)
     * 
     */
    @JsonProperty("lmem_capacity")
    public void setLmemCapacity(Double lmemCapacity) {
        this.lmemCapacity = lmemCapacity;
    }

    /**
     * In GB/s
     * (Required)
     * 
     */
    @JsonProperty("lmem_bandwidth")
    public Double getLmemBandwidth() {
        return lmemBandwidth;
    }

    /**
     * In GB/s
     * (Required)
     * 
     */
    @JsonProperty("lmem_bandwidth")
    public void setLmemBandwidth(Double lmemBandwidth) {
        this.lmemBandwidth = lmemBandwidth;
    }

    @JsonProperty("lmem_max_clock_rate")
    public Double getLmemMaxClockRate() {
        return lmemMaxClockRate;
    }

    @JsonProperty("lmem_max_clock_rate")
    public void setLmemMaxClockRate(Double lmemMaxClockRate) {
        this.lmemMaxClockRate = lmemMaxClockRate;
    }

    @JsonProperty("lmem_dimms")
    public Double getLmemDimms() {
        return lmemDimms;
    }

    @JsonProperty("lmem_dimms")
    public void setLmemDimms(Double lmemDimms) {
        this.lmemDimms = lmemDimms;
    }

    @JsonProperty("fmem_type")
    public String getFmemType() {
        return fmemType;
    }

    @JsonProperty("fmem_type")
    public void setFmemType(String fmemType) {
        this.fmemType = fmemType;
    }

    /**
     * In MB
     * 
     */
    @JsonProperty("fmem_capacity")
    public Double getFmemCapacity() {
        return fmemCapacity;
    }

    /**
     * In MB
     * 
     */
    @JsonProperty("fmem_capacity")
    public void setFmemCapacity(Double fmemCapacity) {
        this.fmemCapacity = fmemCapacity;
    }

    /**
     * In TB/s
     * 
     */
    @JsonProperty("fmem_bandwidth")
    public Double getFmemBandwidth() {
        return fmemBandwidth;
    }

    /**
     * In TB/s
     * 
     */
    @JsonProperty("fmem_bandwidth")
    public void setFmemBandwidth(Double fmemBandwidth) {
        this.fmemBandwidth = fmemBandwidth;
    }

}
