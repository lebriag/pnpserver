/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cloudlightning.plugandplay.clresourcecollector;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.Channel;
import eu.cloudlightning.bcri.clresource.CLResource;
import eu.cloudlightning.bcri.event.EventResourceRegistration;
import eu.cloudlightning.plugandplay.clresourcestorage.PlugAndPlayDBConnector;
import eu.cloudlightning.plugandplay.masks.AbstractMask;
import eu.cloudlightning.plugandplay.resourcemodel.ResourceDescription;
import eu.cloudlightning.util.QueueMessageClient;
import eu.cloudlightning.util.QueueMessageForwarder;

import java.io.InputStream;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author gabriel
 */
public class SOSMRegister implements Runnable {
    
    ClResourceCollectorService pnp_server = null;
    final AsyncResponse response;
    InputStream incomingData;
    String COMPONENTID;
    AbstractMask mask;
    QueueMessageForwarder forwarder;
    QueueMessageClient listener;
    private static PlugAndPlayDBConnector dbConnector;
    
    public SOSMRegister(final AsyncResponse response, InputStream incomingData, ClResourceCollectorService pnp_server, String id, AbstractMask mask,
    		QueueMessageForwarder forwarder,QueueMessageClient listener,PlugAndPlayDBConnector dbConnector)
    {
        this.pnp_server = pnp_server;
        this.response = response;
        this.incomingData = incomingData;  
        this.COMPONENTID = id;
        this.mask = mask;
        this.forwarder = forwarder;
        this.listener = listener;
        this.dbConnector = dbConnector;
    }
    
    @Override
    public void run() {

    	String failureStr = "Mapping the input";
        String cl_Resource_id = ObjectId.get().toString();
        CLResource clResource = null;
        try {

            System.out.println("Message from plugin received");

            // 1. create class objects with Gson
            ObjectMapper mapper = new ObjectMapper();
            ResourceDescription resource = new ResourceDescription();
            resource = mapper.readValue(incomingData, ResourceDescription.class);
            failureStr = "Applying resource grouping strategy";
            clResource = mask.createCLResource(resource, COMPONENTID, cl_Resource_id);
            
            if (clResource != null){
                System.out.println("Registration request parsed OK - CL-Resource creation success");

                // Create the event
                    EventResourceRegistration event = new EventResourceRegistration();
                    event.setEntityId(COMPONENTID);
                    event.setEventType("EVENT_RESOURCE_REGISTRATION");
                    event.setTimestamp(clResource.getTimestamp());
                    event.setClResourceType(clResource.getClResourceId());
//                    if ( ((clResource.getResourceType()).equals("OPENSTACK_RESOURCE")) || 
//                    ((clResource.getResourceType()).equals("OPENSTACK_ACCOUNT"))
//                    ){
////                    	cl_Resource_id = clResource.getTelemetryEndpoint().replaceAll("http://","").replaceAll("https://","").replace(".", "-").replace(":","_") + "@" + clResource.getGenericResourceUnits().get(0).getServerName();
//                    	cl_Resource_id = clResource.getGenericResourceUnits().get(0).getServerName();
//
//                    }
                    clResource.setClResourceId(cl_Resource_id);
                    event.setClResource(clResource);
                    final String clResourceID = clResource.getClResourceId();
                
                // Send the CL-Resource to SOSM-system
                    ObjectMapper mapper2 = new ObjectMapper();// XXXX testing
                    String payloadStr  = mapper2.writeValueAsString(event);// XXX testing
                    
//                    Gson gson = new GsonBuilder().create();
//                    String payloadStr = gson.toJson(event);
                
                // DEBUG -- print of the event
                    System.out.println(payloadStr);
                    
                // forward the message to the Cell manager
                    forwarder.sendMetrics(payloadStr);     

                // DEBUG -- print to know that the message has been sent
                    System.out.println(" [*] Waiting for ack from SOSM for "+ clResource.getClResourceId());
               
                // Create the listener
                 // Create the message consumer
                    
                    Channel channel = listener.createChannel(); 
                    MessageConsumer consumer = new MessageConsumer(channel, clResource.getClResourceId().toString(), pnp_server, response, false);
                    consumer.getChannel().basicConsume(listener.getRABBITMQ_QNAME_RECEPTION(),true , consumer);
                    
                    // wait for the consumer to finish
                    while (true){
                        if(consumer.getLastMessage() != null){
                            // Add the object_id to force mongo db to store by CL-ID basis
                            JSONParser parser = new JSONParser();
                            JSONObject clResourceObj = (JSONObject) parser.parse(mapper.writeValueAsString(resource));
                            Object dbkey = "_id";
                            Object dbvalue = clResource.getClResourceId().toString(); 
                            clResourceObj.put(dbkey, dbvalue);
                            Object clidkey = "clId";
                            clResourceObj.put(clidkey, dbvalue);
                        	 // Store into the DB
                            if (dbConnector.register(clResourceObj)){
                                // return HTTP response 200 in case of success                      
                            	 System.out.println("PnP Success storing in the DB the CL-Resource id: " + clResource.getClResourceId());                             	  
                            } else {
                           	 	 System.out.println("PnP Error - impossible to store CL-Resource id: " + clResource.getClResourceId());   
                           	 	pnp_server.saveTemporary(payloadStr,clResource.getClResourceId());
                            } 
                        	break;
                        }

                        try {
                          Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                
            }
            else { // Error
            	System.out.println("Unexpected error at the creation of the CL-Resource");
            	pnp_server.submitResponse(response, "", 404);
            }

        } catch (Exception e) {
            System.out.println("PnP - Error Connecting to the Communication Channel: " + failureStr + "\nMessage:"+ e.getMessage());
            pnp_server.submitResponse(response, "", 409);
        }

    }
   
    
}
