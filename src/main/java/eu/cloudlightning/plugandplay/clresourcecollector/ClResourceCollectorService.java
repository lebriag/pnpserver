/*
 * Restful server to collect data from the plug and play interfaces. It provides to methods within the API, @POST, and @DELETE
 */
package eu.cloudlightning.plugandplay.clresourcecollector;

import eu.cloudlightning.properties.PropertiesConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import eu.cloudlightning.bcri.clresource.CLResource;
import eu.cloudlightning.bcri.event.EventResourceDeregistrationResponse;
import eu.cloudlightning.bcri.event.EventResourceRegistration;
import eu.cloudlightning.bcri.event.EventResourceRegistrationResponse;
import eu.cloudlightning.bcri.event.EventType;
import eu.cloudlightning.plugandplay.clresourcestorage.PlugAndPlayDBConnector;
import eu.cloudlightning.plugandplay.masks.AbstractMask;
import eu.cloudlightning.plugandplay.resourcemodel.ResourceDescription;
import eu.cloudlightning.util.QueueMessageClient;
import eu.cloudlightning.util.QueueMessageForwarder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.Timestamp;
import java.util.Properties;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

@Path("/")
/**
 *
 * @author gabriel
 */
public class ClResourceCollectorService {

    private static String DBHOST;
    private static String DBNAME;
    private static String DBPORT;
    private static String DBPROTOCOL;
    private static String DBUSERPWD;
    private static String DBUSERID;
    private static Properties config;
    private static String COMPONENTID;
    private static QueueMessageForwarder forwarder;   
    private static QueueMessageClient listener; 
    private static AbstractMask mask;
    private static PlugAndPlayDBConnector dbConnector;
    
    static {
        config = new Properties();
        try {
          InputStream stream = new FileInputStream(PropertiesConstants.CONFIGURATION);
          try {
            config.load(stream);
            
            COMPONENTID = config.getProperty(PropertiesConstants.COMPONENTID_KEY);
            
            // configuration of the rabbitMQ parameters             
            forwarder = new QueueMessageForwarder(config);
            
            // Connect to the rabbitMQ listener
            listener = new QueueMessageClient(config);
            listener.connectToQueue();
            
            // configuration of the Database and the rest parameters
            DBHOST = config.getProperty(PropertiesConstants.DBHOST_KEY);
            DBNAME = config.getProperty(PropertiesConstants.DBNAME_KEY);
            DBPORT = config.getProperty(PropertiesConstants.DBPORT_KEY);
            DBPROTOCOL = config.getProperty(PropertiesConstants.DBPROTOCOL_KEY);
            DBUSERID = config.getProperty(PropertiesConstants.DBUSERAUTH_KEY);
            DBUSERPWD = config.getProperty(PropertiesConstants.DBPASSWORDAUTH_KEY);
            config.getProperty(PropertiesConstants.REQUEST_TIMEOUT_KEY);
            
            // Connector to the DB
            dbConnector = new PlugAndPlayDBConnector(DBUSERID, DBUSERPWD, DBHOST, DBPORT, DBNAME, DBPROTOCOL);
           
            // Get the mask type
            String maskClassName = config.getProperty("maskclass");
            
            try {
                Class c = Class.forName(maskClassName);  // Dynamically load the class
                Object o = c.newInstance();              // Dynamically instantiate it
                mask = (AbstractMask) o;
            } catch (Exception e) { 
                     throw new RuntimeException("It is not possible to find maskclass:" + maskClassName, e);  
            }

          }
          finally {
            stream.close();
          }
        }
        catch (Exception ex) {
          throw new RuntimeException("Could not init class. Error in the connections to DB or Communication channels.", ex);
        }
    }
    
    public void submitResponse(AsyncResponse response, String clResourceID, int status){
    	response.resume(Response.status(status).entity(clResourceID).build());
    }
    
    public void saveTemporary(String input, String clId) throws UnsupportedEncodingException, FileNotFoundException, IOException{
    	// Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    	try (Writer writer = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(clId), "utf-8"))) {writer.write(input);}
    }
    
    @POST    
    @Path("/resource_fabric")
    @Consumes(MediaType.APPLICATION_JSON)
    public void registerResource(@Suspended final AsyncResponse response, InputStream incomingData) {
    	SOSMRegister thr = new SOSMRegister(response, incomingData, this, COMPONENTID, mask, forwarder, listener, dbConnector);
    	thr.run();       
    }
    
    @DELETE    
    @Path("/resource_fabric/{id}")
    @Produces("text/plain")
    public void unregisterResource(@Suspended final AsyncResponse response, @PathParam("id") String clresourceid, InputStream incomingData) {
    	SOSMDeregister thr = new SOSMDeregister(response, incomingData, clresourceid, this, COMPONENTID, mask, forwarder, listener, dbConnector);
    	thr.run();    
    }


  
}
