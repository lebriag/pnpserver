
package eu.cloudlightning.plugandplay.clresourcecollector;

import eu.cloudlightning.properties.PropertiesCollector;
import eu.cloudlightning.restserver.RestServer;
import org.glassfish.grizzly.http.server.HttpServer;
import java.io.IOException;

/**
 * Main class
 */
public class Main{    
  
    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        PropertiesCollector properties = new PropertiesCollector();
        final HttpServer server = RestServer.startServer();

        System.in.read();

        server.stop();        

    }
}