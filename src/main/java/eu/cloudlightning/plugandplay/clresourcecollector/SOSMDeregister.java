/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cloudlightning.plugandplay.clresourcecollector;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import eu.cloudlightning.bcri.clresource.CLResource;
import eu.cloudlightning.bcri.event.EventResourceDeregistrationResponse;
import eu.cloudlightning.bcri.event.EventResourceRegistration;
import eu.cloudlightning.plugandplay.clresourcestorage.PlugAndPlayDBConnector;
import eu.cloudlightning.plugandplay.masks.AbstractMask;
import eu.cloudlightning.plugandplay.resourcemodel.ResourceDescription;
import eu.cloudlightning.util.QueueMessageClient;
import eu.cloudlightning.util.QueueMessageForwarder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author gabriel
 */
public class SOSMDeregister implements Runnable {
    
    ClResourceCollectorService pnp_server = null;
    final AsyncResponse response;
    InputStream incomingData;
    String COMPONENTID;
    String clresourceid;
    AbstractMask mask;
    QueueMessageForwarder forwarder;
    QueueMessageClient listener;
    private static PlugAndPlayDBConnector dbConnector;
    
    public SOSMDeregister(final AsyncResponse response, InputStream incomingData, String clresourceid, ClResourceCollectorService pnp_server, String id, AbstractMask mask,
    		QueueMessageForwarder forwarder,QueueMessageClient listener,PlugAndPlayDBConnector dbConnector)
    {
        this.pnp_server = pnp_server;
        this.response = response;
        this.incomingData = incomingData;  
        this.COMPONENTID = id;
        this.mask = mask;
        this.forwarder = forwarder;
        this.listener = listener;
        SOSMDeregister.dbConnector = dbConnector;
        this.clresourceid = clresourceid;
    }
    
    @Override
    public void run() {

    	 // 1. create an event for the SOMS system
         try{     
             JSONObject sosmRequestBuilder = new JSONObject(); 

             sosmRequestBuilder.put("eventType", "EVENT_RESOURCE_DEREGISTRATION");
             sosmRequestBuilder.put("entityId", COMPONENTID);
//             sosmRequestBuilder.put("timestamp", System.currentTimeMillis());
             sosmRequestBuilder.put("timestamp", "" + System.currentTimeMillis() / 1000);
             sosmRequestBuilder.put("clResourceId", clresourceid);

             System.out.print(sosmRequestBuilder.toString());
             String objevent = dbConnector.access(clresourceid); 
             boolean dbaccess = false;
             if (!new String (objevent).equals("null")) 
            	 dbaccess = true;
             
             if (!dbaccess){
             	System.out.println("PnP - CLResourceid[" + clresourceid + "] is not in the DB or DB connection problems. Searching in local storage..");
            
             	// try to find into local storage..
             	File f = new File("./");
                File[] matchingFiles = f.listFiles(new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        return name.startsWith(clresourceid);
                    }
                });
                if (matchingFiles.length == 0){
                    System.out.println("PnP Error - impossible to remove CL-Resource: " + clresourceid);   
                    pnp_server.submitResponse(response, clresourceid, 404);
                }
                else {
                	// files are in local storage
                	dbaccess =  true;
                }
             }
             
             // if files are in local storage or DB.
             if (dbaccess){
	                 // send to the SOSM system
	                 forwarder.sendMetrics(sosmRequestBuilder.toString());                       

                
                	// DEBUG -- print to know that the message has been sent
                     System.out.println(" [*] Waiting for ack from SOSM for unregister: "+ clresourceid);
                
                 // Create the listener
                  // Create the message consumer
                     
                     Channel channel = listener.createChannel(); 
                     MessageConsumer consumer = new MessageConsumer(channel, clresourceid, pnp_server, response, true);
                     consumer.getChannel().basicConsume(listener.getRABBITMQ_QNAME_RECEPTION(),true , consumer);
                     
                     // wait for the consumer to finish
                     while (true){
                         if(consumer.getStatus() != 0){
                         	
                         	 // Store into the DB
                             if (dbConnector.unregister(clresourceid)){
                                 // return HTTP response 200 in case of success                      
                             	 System.out.println("PnP Success removing CL-Resource id: " + clresourceid + " from the DB");                             	  
                             	 //pnp_server.resourceRegistered(response, clResource.getClResourceId(), consumer.getStatus());
                             } else {
                                 File file = new File("./"+clresourceid);
                                 if(file.delete()){
                            		 System.out.println("PnP Success removing CL-Resource id: " + clresourceid + " from the DB");     
                            	 } else {
                            		 System.out.println("PnP Error - System went down during the request.. " + clresourceid);   
                            	 }
                             } 
                    		 pnp_server.submitResponse(response, "", consumer.getStatus());

                         	break;
                         }

                         try {
                           Thread.sleep(1000);
                         } catch (InterruptedException e) {
                             e.printStackTrace();
                         }
                     }

             }
        } catch (Exception e) {
            System.out.println("PnP - Error processing request: \nMessage: "+ e.getMessage());
            pnp_server.submitResponse(response, "", 409);
        }
    }
    
}
