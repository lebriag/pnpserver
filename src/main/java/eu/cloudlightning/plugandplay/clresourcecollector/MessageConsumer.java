package eu.cloudlightning.plugandplay.clresourcecollector;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

import eu.cloudlightning.bcri.event.EventResourceDeregistrationResponse;
import eu.cloudlightning.bcri.event.EventResourceRegistrationResponse;

/**
 * Created by gabriel
 */
public class MessageConsumer extends DefaultConsumer {

    private EventResourceRegistrationResponse lastMessage;
    private AsyncResponse response;
    private boolean success;
    private String cl_Resource_id = "";
    ClResourceCollectorService pnp_server;
    private int status;
    private boolean isDeregistration;
    
    public MessageConsumer(Channel channel, String clResourceID,  ClResourceCollectorService pnp_server, AsyncResponse response, boolean isDeregistration){
        super(channel);
        this.lastMessage = null;
        this.response = response;
        this.success = false;
        this.cl_Resource_id = clResourceID;
        this.pnp_server = pnp_server;
        this.isDeregistration = isDeregistration;
        this.status = 0;
    }

    @Override
    public void handleConsumeOk(String s) {

    }

    @Override
    public void handleCancelOk(String s) {

    }

    @Override
    public void handleCancel(String s) throws IOException {

    }

    @Override
    public void handleShutdownSignal(String s, ShutdownSignalException e) {

    }

    @Override
    public void handleRecoverOk(String s) {

    }

    @Override
    public void handleDelivery(String s, Envelope envelope, AMQP.BasicProperties basicProperties, byte[] body) throws IOException {
    	 String message = new String(body, "UTF-8");
         System.out.println(" [x] Received '" + message + "'");
         // Check if the message is mine
         ObjectMapper mapper = new ObjectMapper();
         EventResourceRegistrationResponse event_response;
         EventResourceDeregistrationResponse event_dResponse;
        
         if (!isDeregistration) {
        	 event_response = mapper.readValue(message, EventResourceRegistrationResponse.class);
        	// it is for me!
             if (event_response.getClResourceId().equals(cl_Resource_id)){
            	        	         	 
                 if (event_response.getStatus().equals("SUCCESSFUL")){
                	 status = 200;            			 
                	 setSuccess(true);
                 }
                 else 
                	 status = 407;
             
                 setLastMessage(event_response);
                 pnp_server.submitResponse(response, cl_Resource_id, this.getStatus());
             }
             else{
            	 System.out.println("WTF!! - " + event_response.getClResourceId() + " - " + cl_Resource_id );
            	 status = -1;
             }
         }else{
        	 event_dResponse = mapper.readValue(message, EventResourceDeregistrationResponse.class);
        	// it is for me!
             if (event_dResponse.getClResourceId().equals(cl_Resource_id)){
            	        	         	 
                 if (event_dResponse.getStatus().equals("SUCCESSFUL")){
                	 status = 200;            			 
                	 setSuccess(true);
                 }
                 else 
                	 status = 407;
             
//                 setLastMessage(event_response);
                 pnp_server.submitResponse(response, cl_Resource_id, this.getStatus());
             }
             else{
            	 System.out.println("WTF!! - " + event_dResponse.getClResourceId() + " - " + cl_Resource_id );
            	 status = -1;
             }
         }
    	 
         
         
        super.getChannel().basicAck(envelope.getDeliveryTag(), false);

    }

	public boolean isSuccess() {
		return success;
	}

	private void setSuccess(boolean success) {
		this.success = success;
	}

	private void setLastMessage(EventResourceRegistrationResponse lastMessage) {
		this.lastMessage = lastMessage;
	}
    
    public EventResourceRegistrationResponse getLastMessage() {
        return lastMessage;
    }

	public ClResourceCollectorService getPnp_server() {
		return pnp_server;
	}

	public void setPnp_server(ClResourceCollectorService pnp_server) {
		this.pnp_server = pnp_server;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
    
    
}
