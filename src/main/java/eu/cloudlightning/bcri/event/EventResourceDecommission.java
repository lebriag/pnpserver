package eu.cloudlightning.bcri.event;

import java.util.ArrayList;
import java.util.List;

import eu.cloudlightning.bcri.blueprint.ResourcedServiceElement;

public class EventResourceDecommission {
    private String entityId;
    private String eventType;
    private String blueprintId;
    private long timestamp;
    private List<String> targets;
    private ResourcedServiceElement resourcedServiceElement;

    public EventResourceDecommission() {
	this.entityId = "";
	this.eventType = EventType.EVENT_RESOURCE_DECOMMISSION.name();
	this.timestamp = 0;
	this.targets = new ArrayList<String>();
	this.blueprintId = "";
	this.resourcedServiceElement = new ResourcedServiceElement();
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
	return entityId;
    }

    /**
     * @param entityId
     *            the entityId to set
     */
    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
	return eventType;
    }

    /**
     * @param eventType
     *            the eventType to set
     */
    public void setEventType(String eventType) {
	this.eventType = eventType;
    }

    /**
     * @return the blueprintId
     */
    public String getBlueprintId() {
	return blueprintId;
    }

    /**
     * @param blueprintId
     *            the blueprintId to set
     */
    public void setBlueprintId(String blueprintId) {
	this.blueprintId = blueprintId;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
	return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(long timestamp) {
	this.timestamp = timestamp;
    }

    /**
     * @return the targets
     */
    public List<String> getTargets() {
	return targets;
    }

    /**
     * @param targets
     *            the targets to set
     */
    public void setTargets(List<String> targets) {
	this.targets = targets;
    }

    /**
     * @return the resourcedServiceElement
     */
    public ResourcedServiceElement getResourcedServiceElement() {
	return resourcedServiceElement;
    }

    /**
     * @param resourcedServiceElement
     *            the resourcedServiceElement to set
     */
    public void setResourcedServiceElement(ResourcedServiceElement resourcedServiceElement) {
	this.resourcedServiceElement = resourcedServiceElement;
    }

}