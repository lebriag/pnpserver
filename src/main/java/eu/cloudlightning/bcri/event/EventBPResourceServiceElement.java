package eu.cloudlightning.bcri.event;

import java.util.ArrayList;
import java.util.List;

import eu.cloudlightning.bcri.blueprint.Implementation;

public class EventBPResourceServiceElement {
    private String blueprintId;
    private String serviceElementId;
    private long timestamp;
    private String eventType;
    private Implementation implementation;
    private List<String> targets;

    public EventBPResourceServiceElement() {
	this.blueprintId = "";
	this.serviceElementId = "";
	this.timestamp = 0;
	this.eventType = EventType.EVENT_BP_RESOURCE_SERVICE_ELEMENT.name();
	this.implementation = new Implementation();
	this.targets = new ArrayList<String>();
    }

    /**
     * @return the blueprintId
     */
    public String getBlueprintId() {
	return blueprintId;
    }

    /**
     * @param blueprintId
     *            the blueprintId to set
     */
    public void setBlueprintId(String blueprintId) {
	this.blueprintId = blueprintId;
    }

    /**
     * @return the serviceElementId
     */
    public String getServiceElementId() {
	return serviceElementId;
    }

    /**
     * @param serviceElementId
     *            the serviceElementId to set
     */
    public void setServiceElementId(String serviceElementId) {
	this.serviceElementId = serviceElementId;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
	return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(long timestamp) {
	this.timestamp = timestamp;
    }

    /**
     * @return the implementation
     */
    public Implementation getImplementation() {
	return implementation;
    }

    /**
     * @param implementation
     *            the implementation to set
     */
    public void setImplementation(Implementation implementation) {
	this.implementation = implementation;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
	return eventType;
    }

    /**
     * @param eventType
     *            the eventType to set
     */
    public void setEventType(String eventType) {
	this.eventType = eventType;
    }

    /**
     * @return the targets
     */
    public List<String> getTargets() {
	return targets;
    }

    /**
     * @param targets
     *            the targets to set
     */
    public void setTargets(List<String> targets) {
	this.targets = targets;
    }
}
