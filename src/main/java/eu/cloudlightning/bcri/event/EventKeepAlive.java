package eu.cloudlightning.bcri.event;

import java.util.ArrayList;
import java.util.List;

public class EventKeepAlive {
    private String entityId;
    private long timestamp;
    private String eventType;
    private int managedElement;
    private List<String> targets;
    private boolean status;

    public EventKeepAlive() {
	this.entityId = "";
	this.timestamp = 0;
	this.eventType = EventType.EVENT_KEEPALIVE.name();
	this.targets = new ArrayList<String>();
	this.managedElement = 0;
	this.status = false;
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
	return entityId;
    }

    /**
     * @param entityId
     *            the entityId to set
     */
    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
	return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(long timestamp) {
	this.timestamp = timestamp;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
	return eventType;
    }

    /**
     * @param eventType
     *            the eventType to set
     */
    public void setEventType(String eventType) {
	this.eventType = eventType;
    }

    /**
     * @return the targets
     */
    public List<String> getTargets() {
	return targets;
    }

    /**
     * @param targets
     *            the targets to set
     */
    public void setTargets(List<String> targets) {
	this.targets = targets;
    }

    /**
     * @return the managedElement
     */
    public int getManagedElement() {
	return managedElement;
    }

    /**
     * @param managedElement
     *            the managedElement to set
     */
    public void setManagedElement(int managedElement) {
	this.managedElement = managedElement;
    }

    public boolean getStatus() {
	return status;
    }

    public void setStatus(boolean status) {
	this.status = status;
    }
}