package eu.cloudlightning.bcri.event;

import java.util.ArrayList;
import java.util.List;

import eu.cloudlightning.bcri.blueprint.ServiceImplementationType;
import eu.cloudlightning.bcri.clresource.CLResource;

public class EventResourceRegistration extends AbstractEventModel {
    private String entityId;
    private String eventType;
    private double timestamp;
    private String clResourceType;
    private List<String> targets;

    private CLResource clResource;

    public EventResourceRegistration() {
	this.entityId = "";
	this.eventType = EventType.EVENT_RESOURCE_REGISTRATION.name();
	this.timestamp = 0.0;
	this.clResourceType = ServiceImplementationType.CPU_VM.name();
	this.clResource = new CLResource();
	this.targets = new ArrayList<String>();
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
	return entityId;
    }

    /**
     * @param entityId
     *            the entityId to set
     */
    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
	return eventType;
    }

    /**
     * @param eventType
     *            the eventType to set
     */
    public void setEventType(String eventType) {
	this.eventType = eventType;
    }

    /**
     * @return the clResourceType
     */
    public String getClResourceType() {
	return clResourceType;
    }

    /**
     * @param clResourceType
     *            the clResourceType to set
     */
    public void setClResourceType(String clResourceType) {
	this.clResourceType = clResourceType;
    }

    /**
     * @return the timestamp
     */
    public double getTimestamp() {
	return timestamp;
    }

    /**
     * @param string
     *            the timestamp to set
     */
    public void setTimestamp(double timestamp) {
	this.timestamp = timestamp;
    }

    /**
     * @return the clResource
     */
    public CLResource getClResource() {
	return clResource;
    }

    /**
     * @param clResource
     *            the clResource to set
     */
    public void setClResource(CLResource clResource) {
	this.clResource = clResource;
    }

    /**
     * @return the targets
     */
    public List<String> getTargets() {
	return targets;
    }

    /**
     * @param targets
     *            the targets to set
     */
    public void setTargets(List<String> targets) {
	this.targets = targets;
    }

}
