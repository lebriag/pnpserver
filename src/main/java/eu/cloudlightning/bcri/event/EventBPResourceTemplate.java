package eu.cloudlightning.bcri.event;

import java.util.ArrayList;
import java.util.List;

import eu.cloudlightning.bcri.blueprint.ProcessingStatus;
import eu.cloudlightning.bcri.blueprint.ServiceElement;

public class EventBPResourceTemplate {
    private String blueprintId;
    private long timestamp;
    private double cost;
    private String callbackEndpoint;
    private List<ServiceElement> serviceElements;
    private String entityId;
    private String eventType;
    private String status;

    public EventBPResourceTemplate() {
	this.blueprintId = "";
	this.timestamp = 0;
	this.cost = 0;
	this.callbackEndpoint = "";
	this.serviceElements = new ArrayList<ServiceElement>();
	this.entityId = "";
	this.eventType = EventType.EVENT_BP_RESOURCE_TEMPLATE.name();
	this.status = ProcessingStatus.PENDING.name();
    }

    /**
     * @return the blueprintId
     */
    public String getBlueprintId() {
	return blueprintId;
    }

    /**
     * @param blueprintId
     *            the blueprintId to set
     */
    public void setBlueprintId(String blueprintId) {
	this.blueprintId = blueprintId;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
	return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(long timestamp) {
	this.timestamp = timestamp;
    }

    /**
     * @return the cost
     */
    public double getCost() {
	return cost;
    }

    /**
     * @param cost
     *            the cost to set
     */
    public void setCost(double cost) {
	this.cost = cost;
    }

    /**
     * @return the callbackEndpoint
     */
    public String getCallbackEndpoint() {
	return callbackEndpoint;
    }

    /**
     * @param callbackEndpoint
     *            the callbackEndpoint to set
     */
    public void setCallbackEndpoint(String callbackEndpoint) {
	this.callbackEndpoint = callbackEndpoint;
    }

    /**
     * @return the serviceElements
     */
    public List<ServiceElement> getServiceElements() {
	return serviceElements;
    }

    /**
     * @param serviceElements
     *            the serviceElements to set
     */
    public void setServiceElements(List<ServiceElement> serviceElements) {
	this.serviceElements = serviceElements;
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
	return entityId;
    }

    /**
     * @param entityId
     *            the entityId to set
     */
    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
	return eventType;
    }

    /**
     * @param eventType
     *            the eventType to set
     */
    public void setEventType(String eventType) {
	this.eventType = eventType;
    }

    /**
     * @return the status
     */
    public String getStatus() {
	return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
	this.status = status;
    }
}
