package eu.cloudlightning.bcri.event;

import java.util.ArrayList;
import java.util.List;

public class EventResourceDeregistration {
    private String entityId;
    private String eventType;
    private long timestamp;
    private String clResourceId;
    private List<String> targets;

    public EventResourceDeregistration() {
	this.entityId = "";
	this.eventType = EventType.EVENT_RESOURCE_DEREGISTRATION.name();
	this.timestamp = 0;
	this.clResourceId = "";
	this.targets = new ArrayList<String>();
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
	return entityId;
    }

    /**
     * @param entityId
     *            the entityId to set
     */
    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
	return eventType;
    }

    /**
     * @param eventType
     *            the eventType to set
     */
    public void setEventType(String eventType) {
	this.eventType = eventType;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
	return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(long timestamp) {
	this.timestamp = timestamp;
    }

    /**
     * @return the clResourceId
     */
    public String getClResourceId() {
	return clResourceId;
    }

    /**
     * @param clResourceId
     *            the clResourceId to set
     */
    public void setClResourceId(String clResourceId) {
	this.clResourceId = clResourceId;
    }

    /**
     * @return the targets
     */
    public List<String> getTargets() {
	return targets;
    }

    /**
     * @param targets
     *            the targets to set
     */
    public void setTargets(List<String> targets) {
	this.targets = targets;
    }
}
