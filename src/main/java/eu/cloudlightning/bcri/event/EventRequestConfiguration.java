package eu.cloudlightning.bcri.event;

import java.util.ArrayList;
import java.util.List;

public class EventRequestConfiguration {
    private String entityId;
    private String eventType;
    private long timestamp;
    private List<String> targets;

    public EventRequestConfiguration() {
	this.entityId = "";
	this.eventType = EventType.EVENT_REQUEST_CONFIGURATION.name();
	this.timestamp = 0;
	this.targets = new ArrayList<String>();
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
	return entityId;
    }

    /**
     * @param entityId
     *            the entityId to set
     */
    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
	return eventType;
    }

    /**
     * @param eventType
     *            the eventType to set
     */
    public void setEventType(String eventType) {
	this.eventType = eventType;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
	return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(long timestamp) {
	this.timestamp = timestamp;
    }

    /**
     * @return the targets
     */
    public List<String> getTargets() {
	return targets;
    }

    /**
     * @param targets
     *            the targets to set
     */
    public void setTargets(List<String> targets) {
	this.targets = targets;
    }
}
