package eu.cloudlightning.bcri.event;

public class EventResourceDeregistrationResponse extends AbstractEventModel{
    private String entityId;
    private String clResourceId;
    private String status;
    private long timestamp;
    private String eventType;

    public EventResourceDeregistrationResponse() {
	this.entityId = "";
	this.clResourceId = "";
	this.status = "";
	this.timestamp = 0;
	this.eventType = EventType.EVENT_RESOURCE_DEREGISTRATION_RESPONSE.name();
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
	return entityId;
    }

    /**
     * @param entityId
     *            the entityId to set
     */
    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }

    /**
     * @return the clResourceId
     */
    public String getClResourceId() {
	return clResourceId;
    }

    /**
     * @param clResourceId
     *            the clResourceId to set
     */
    public void setClResourceId(String clResourceId) {
	this.clResourceId = clResourceId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
	return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
	this.status = status;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
	return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(long timestamp) {
	this.timestamp = timestamp;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
	return eventType;
    }

    /**
     * @param eventType
     *            the eventType to set
     */
    public void setEventType(String eventType) {
	this.eventType = eventType;
    }

}
