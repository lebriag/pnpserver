package eu.cloudlightning.bcri.event;

public enum EventQueueType {
    SIMPLE, WORKER, PUBLISHER, TOPICS, ROUTING, RPC;
}
