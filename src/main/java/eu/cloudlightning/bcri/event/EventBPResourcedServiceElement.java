package eu.cloudlightning.bcri.event;

import eu.cloudlightning.bcri.blueprint.ProcessingStatus;
import eu.cloudlightning.bcri.blueprint.ResourcedServiceElement;

public class EventBPResourcedServiceElement {
    private String entityId;
    private String blueprintId;
    private String status;
    private String eventType;
    private ResourcedServiceElement resourcedServiceElement;

    public EventBPResourcedServiceElement() {
	this.entityId = "";
	this.blueprintId = "";
	this.status = ProcessingStatus.PENDING.name();
	this.eventType = EventType.EVENT_BP_RESOURCED_SERVICE_ELEMENT.name();
	this.resourcedServiceElement = new ResourcedServiceElement();
    }

    /**
     * @return the blueprintId
     */
    public String getBlueprintId() {
	return blueprintId;
    }

    /**
     * @param blueprintId
     *            the blueprintId to set
     */
    public void setBlueprintId(String blueprintId) {
	this.blueprintId = blueprintId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
	return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
	this.status = status;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
	return eventType;
    }

    /**
     * @param eventType
     *            the eventType to set
     */
    public void setEventType(String eventType) {
	this.eventType = eventType;
    }

    /**
     * @return the resourcedServiceElement
     */
    public ResourcedServiceElement getResourcedServiceElement() {
	return resourcedServiceElement;
    }

    /**
     * @param resourcedServiceElement
     *            the resourcedServiceElement to set
     */
    public void setResourcedServiceElement(ResourcedServiceElement resourcedServiceElement) {
	this.resourcedServiceElement = resourcedServiceElement;
    }

    public String getEntityId() {
	return entityId;
    }

    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }
}