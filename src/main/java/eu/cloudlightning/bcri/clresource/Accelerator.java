package eu.cloudlightning.bcri.clresource;

import eu.cloudlightning.bcri.blueprint.ProcessingStatus;

public class Accelerator {
    private String acceleratorType; // DFE, MIC, GPU
    private String manufacturer;
    private String model;
    private double memorySize; // MB
    private String memoryType; // e.g., GDDR5, GDDR5X
    private double memoryClock;
    private String busType; // e.g., PCIe 3.0 x16
    private int numOfCores;
    private double frequency;
    private String status;
    private String acceleratorId;


	public Accelerator() {
	this.acceleratorType = "";
	this.manufacturer = "";
	this.model = "";
	this.memoryClock = 0.0;
	this.memorySize = 0.0;
	this.memoryType = "";
	this.busType = "";
	this.numOfCores = 0;
	this.frequency = 1.0;
	this.status = ProcessingStatus.FREE.name();
	this.acceleratorId = "";
    }


    public String getAcceleratorId() {
		return acceleratorId;
	}

	public void setAcceleratorId(String acceleratorId) {
		this.acceleratorId = acceleratorId;
	}
	
    /**
     * @return the acceleratorType
     */
    public String getAcceleratorType() {
	return acceleratorType;
    }

    /**
     * @param acceleratorType
     *            the acceleratorType to set
     */
    public void setAcceleratorType(String acceleratorType) {
	this.acceleratorType = acceleratorType;
    }

    /**
     * @return the manufacturer
     */
    public String getManufacturer() {
	return manufacturer;
    }

    /**
     * @param manufacturer
     *            the manufacturer to set
     */
    public void setManufacturer(String manufacturer) {
	this.manufacturer = manufacturer;
    }

    /**
     * @return the model
     */
    public String getModel() {
	return model;
    }

    /**
     * @param model
     *            the model to set
     */
    public void setModel(String model) {
	this.model = model;
    }

    /**
     * @return the memorySize
     */
    public double getMemorySize() {
	return memorySize;
    }

    /**
     * @param memorySize
     *            the memorySize to set
     */
    public void setMemorySize(double memorySize) {
	this.memorySize = memorySize;
    }

    /**
     * @return the memoryType
     */
    public String getMemoryType() {
	return memoryType;
    }

    /**
     * @param memoryType
     *            the memoryType to set
     */
    public void setMemoryType(String memoryType) {
	this.memoryType = memoryType;
    }

    /**
     * @return the memoryClock
     */
    public double getMemoryClock() {
	return memoryClock;
    }

    /**
     * @param memoryClock
     *            the memoryClock to set
     */
    public void setMemoryClock(double memoryClock) {
	this.memoryClock = memoryClock;
    }

    /**
     * @return the busType
     */
    public String getBusType() {
	return busType;
    }

    /**
     * @param busType
     *            the busType to set
     */
    public void setBusType(String busType) {
	this.busType = busType;
    }

    /**
     * @return the numOfCores
     */
    public int getNumOfCores() {
	return numOfCores;
    }

    /**
     * @param numOfCores
     *            the numOfCores to set
     */
    public void setNumOfCores(int numOfCores) {
	this.numOfCores = numOfCores;
    }

    /**
     * @return the frequency
     */
    public double getFrequency() {
	return frequency;
    }

    /**
     * @param frequency
     *            the frequency to set
     */
    public void setFrequency(double frequency) {
	this.frequency = frequency;
    }

    /**
     * @return the status
     */
    public String getStatus() {
	return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
	this.status = status;
    }

}
