package eu.cloudlightning.bcri.clresource;

public class Storage {
    private String storageType; // local, remote
    private double totalSize; // GB
    private double currentAvailableSize;
    private double rpm;

    /**
     * @return the storageType
     */
    public String getStorageType() {
	return storageType;
    }

    /**
     * @param storageType
     *            the storageType to set
     */
    public void setStorageType(String storageType) {
	this.storageType = storageType;
    }

    /**
     * @return the totalSize
     */
    public double getTotalSize() {
	return totalSize;
    }

    /**
     * @param totalSize
     *            the totalSize to set
     */
    public void setTotalSize(double totalSize) {
	this.totalSize = totalSize;
    }

    /**
     * @return the currentAvailableSize
     */
    public double getCurrentAvailableSize() {
	return currentAvailableSize;
    }

    /**
     * @param currentAvailableSize
     *            the currentAvailableSize to set
     */
    public void setCurrentAvailableSize(double currentAvailableSize) {
	this.currentAvailableSize = currentAvailableSize;
    }

    /**
     * @return the rpm
     */
    public double getRpm() {
	return rpm;
    }

    /**
     * @param rpm
     *            the rpm to set
     */
    public void setRpm(double rpm) {
	this.rpm = rpm;
    }

}
