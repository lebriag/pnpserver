package eu.cloudlightning.bcri.clresource;

public class CPU {
    private String model;
    private String manufacturer;
    private String processorType; // Mobile, Desktop, Server
    private int cacheSize;
    private int totalNumOfProcessor;
    private int totalNumOfCores;
    private double frequency;
    private int busWidth; // 32-bit, 64-bit
    private int currentAvailableCores;

    public CPU() {
	this.model = "";
	this.manufacturer = "";
	this.processorType = "";
	this.cacheSize = 0;
	this.totalNumOfCores = 0;
	this.totalNumOfProcessor = 0;
	this.frequency = 0.0;
	this.busWidth = 64;
	this.currentAvailableCores = 0;
    }

    /**
     * @return the model
     */
    public String getModel() {
	return model;
    }

    /**
     * @param model
     *            the model to set
     */
    public void setModel(String model) {
	this.model = model;
    }

    /**
     * @return the manufacturer
     */
    public String getManufacturer() {
	return manufacturer;
    }

    /**
     * @param manufacturer
     *            the manufacturer to set
     */
    public void setManufacturer(String manufacturer) {
	this.manufacturer = manufacturer;
    }

    /**
     * @return the processorType
     */
    public String getProcessorType() {
	return processorType;
    }

    /**
     * @param processorType
     *            the processorType to set
     */
    public void setProcessorType(String processorType) {
	this.processorType = processorType;
    }

    /**
     * @return the cacheSize
     */
    public int getCacheSize() {
	return cacheSize;
    }

    /**
     * @param cacheSize
     *            the cacheSize to set
     */
    public void setCacheSize(int cacheSize) {
	this.cacheSize = cacheSize;
    }

    /**
     * @return the totalNumOfProcessor
     */
    public int getTotalNumOfProcessor() {
	return totalNumOfProcessor;
    }

    /**
     * @param totalNumOfProcessor
     *            the totalNumOfProcessor to set
     */
    public void setTotalNumOfProcessor(int totalNumOfProcessor) {
	this.totalNumOfProcessor = totalNumOfProcessor;
    }

    /**
     * @return the totalNumOfCores
     */
    public int getTotalNumOfCores() {
	return totalNumOfCores;
    }

    /**
     * @param totalNumOfCores
     *            the totalNumOfCores to set
     */
    public void setTotalNumOfCores(int totalNumOfCores) {
	this.totalNumOfCores = totalNumOfCores;
    }

    /**
     * @return the frequency
     */
    public double getFrequency() {
	return frequency;
    }

    /**
     * @param frequency
     *            the frequency to set
     */
    public void setFrequency(double frequency) {
	this.frequency = frequency;
    }

    /**
     * @return the busWidth
     */
    public int getBusWidth() {
	return busWidth;
    }

    /**
     * @param busWidth
     *            the busWidth to set
     */
    public void setBusWidth(int busWidth) {
	this.busWidth = busWidth;
    }

    /**
     * @return the currentAvailableCores
     */
    public int getCurrentAvailableCores() {
	return currentAvailableCores;
    }

    /**
     * @param currentAvailableCores
     *            the currentAvailableCores to set
     */
    public void setCurrentAvailableCores(int currentAvailableCores) {
	this.currentAvailableCores = currentAvailableCores;
    }
}
