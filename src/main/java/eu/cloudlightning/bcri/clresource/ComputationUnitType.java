package eu.cloudlightning.bcri.clresource;

public enum ComputationUnitType {
    CPU, GPU, MIC, DFE;

}
