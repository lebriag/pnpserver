
package eu.cloudlightning.bcri.clresource;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "clResourceId",
    "genericResourceUnits",
    "occupationalStatus",
    "resourceDescriptor",
    "resourceType",
    "telemetryEndpoint",
    "timestamp"
})
public class CLResource {

    @JsonProperty("clResourceId")
    private String clResourceId;
    @JsonProperty("genericResourceUnits")
    private List<GenericResourceUnit> genericResourceUnits = new ArrayList<GenericResourceUnit>();
    @JsonProperty("occupationalStatus")
    private String occupationalStatus;
    @JsonProperty("resourceDescriptor")
    private String resourceDescriptor;
    @JsonProperty("resourceType")
    private String resourceType;
    @JsonProperty("telemetryEndpoint")
    private String telemetryEndpoint;
    @JsonProperty("timestamp")
    private double timestamp;
    
//    @JsonIgnore
//    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public CLResource() {
    }

    /**
     * 
     * @param timestamp
     * @param genericResourceUnits
     * @param resourceDescriptor
     * @param clResourceId
     * @param telemetryEndpoint
     * @param resourceType
     * @param occupationalStatus
     */
    public CLResource(String clResourceId, List<GenericResourceUnit> genericResourceUnits, String occupationalStatus, String resourceDescriptor, String resourceType, String telemetryEndpoint, double timestamp) {
        super();
        this.clResourceId = clResourceId;
        this.genericResourceUnits = genericResourceUnits;
        this.occupationalStatus = "FREE";
        this.resourceDescriptor = resourceDescriptor;
        this.resourceType = resourceType;
        this.telemetryEndpoint = telemetryEndpoint;
        this.timestamp = timestamp;
        this.occupationalStatus = occupationalStatus;
    }

    @JsonProperty("clResourceId")
    public String getClResourceId() {
        return clResourceId;
    }

    @JsonProperty("clResourceId")
    public void setClResourceId(String clResourceId) {
        this.clResourceId = clResourceId;
    }

    @JsonProperty("genericResourceUnits")
    public List<GenericResourceUnit> getGenericResourceUnits() {
        return genericResourceUnits;
    }

    @JsonProperty("genericResourceUnits")
    public void setGenericResourceUnits(List<GenericResourceUnit> genericResourceUnits) {
        this.genericResourceUnits = genericResourceUnits;
    }

    @JsonProperty("occupationalStatus")
    public String getOccupationalStatus() {
        return occupationalStatus;
    }

    @JsonProperty("occupationalStatus")
    public void setOccupationalStatus(String occupationalStatus) {
        this.occupationalStatus = occupationalStatus;
    }

    @JsonProperty("resourceDescriptor")
    public String getResourceDescriptor() {
        return resourceDescriptor;
    }

    @JsonProperty("resourceDescriptor")
    public void setResourceDescriptor(String resourceDescriptorString) {
        this.resourceDescriptor = resourceDescriptorString;
    }

    @JsonProperty("resourceType")
    public String getResourceType() {
        return resourceType;
    }

    @JsonProperty("resourceType")
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @JsonProperty("telemetryEndpoint")
    public String getTelemetryEndpoint() {
        return telemetryEndpoint;
    }

    @JsonProperty("telemetryEndpoint")
    public void setTelemetryEndpoint(String telemetryEndpoint) {
        this.telemetryEndpoint = telemetryEndpoint;
    }
    
    @JsonProperty("timestamp")
    public double getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(double timestamp) {
        this.timestamp = timestamp;
    }


}
