package eu.cloudlightning.bcri.clresource;

import java.util.ArrayList;
import java.util.List;

public class GenericResourceUnit {
    private String genericResourceUnitId;
    private String operatingSystem;
    private String operatingSystemVersion;
    private String serverName;
    private String ipAddress;
    private ServerAccess serverAccess;
    private CPU cpu;
    private Memory memory;
    private List<Storage> storage;
    private List<Network> network;
    private List<Accelerator> accelerator;

    public GenericResourceUnit() {
	this.genericResourceUnitId = "";
	this.operatingSystem = "";
	this.operatingSystemVersion = "";
	this.cpu = new CPU();
	this.memory = new Memory();
	this.storage = new ArrayList<Storage>();
	this.network = new ArrayList<Network>();
	this.accelerator = new ArrayList<Accelerator>();
	this.serverName = "";
	this.ipAddress = "";
	this.serverAccess = new ServerAccess();
    }

    /**
     * @return the genericResourceUnitId
     */
    public String getGenericResourceUnitId() {
	return genericResourceUnitId;
    }

    /**
     * @param genericResourceUnitId
     *            the genericResourceUnitId to set
     */
    public void setGenericResourceUnitId(String genericResourceUnitId) {
	this.genericResourceUnitId = genericResourceUnitId;
    }

    /**
     * @return the operatingSystem
     */
    public String getOperatingSystem() {
	return operatingSystem;
    }

    /**
     * @param operatingSystem
     *            the operatingSystem to set
     */
    public void setOperatingSystem(String operatingSystem) {
	this.operatingSystem = operatingSystem;
    }

    /**
     * @return the operatingSystemVersion
     */
    public String getOperatingSystemVersion() {
	return operatingSystemVersion;
    }

    /**
     * @param operatingSystemVersion
     *            the operatingSystemVersion to set
     */
    public void setOperatingSystemVersion(String operatingSystemVersion) {
	this.operatingSystemVersion = operatingSystemVersion;
    }

    /**
     * @return the cpu
     */
    public CPU getCpu() {
	return cpu;
    }

    /**
     * @param cpu
     *            the cpu to set
     */
    public void setCpu(CPU cpu) {
	this.cpu = cpu;
    }

    /**
     * @return the memory
     */
    public Memory getMemory() {
	return memory;
    }

    /**
     * @param memory
     *            the memory to set
     */
    public void setMemory(Memory memory) {
	this.memory = memory;
    }

    /**
     * @return the storage
     */
    public List<Storage> getStorage() {
	return storage;
    }

    /**
     * @param storage
     *            the storage to set
     */
    public void setStorage(List<Storage> storage) {
	this.storage = storage;
    }

    /**
     * @return the network
     */
    public List<Network> getNetwork() {
	return network;
    }

    /**
     * @param network
     *            the network to set
     */
    public void setNetwork(List<Network> network) {
	this.network = network;
    }

    /**
     * @return the accelerator
     */
    public List<Accelerator> getAccelerator() {
	return accelerator;
    }

    /**
     * @param accelerator
     *            the accelerator to set
     */
    public void setAccelerator(List<Accelerator> accelerator) {
	this.accelerator = accelerator;
    }

    /**
     * @return the serverName
     */
    public String getServerName() {
	return serverName;
    }

    /**
     * @param serverName
     *            the serverName to set
     */
    public void setServerName(String serverName) {
	this.serverName = serverName;
    }

    /**
     * @return the ipAddress
     */
    public String getIpAddress() {
	return ipAddress;
    }

    /**
     * @param ipAddress
     *            the ipAddress to set
     */
    public void setIpAddress(String ipAddress) {
	this.ipAddress = ipAddress;
    }

    /**
     * @return the serverAccess
     */
    public ServerAccess getServerAccess() {
	return serverAccess;
    }

    /**
     * @param serverAccess
     *            the serverAccess to set
     */
    public void setServerAccess(ServerAccess serverAccess) {
	this.serverAccess = serverAccess;
    }

}
