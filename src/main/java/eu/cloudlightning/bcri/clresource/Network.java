package eu.cloudlightning.bcri.clresource;

public class Network {
    private String networkType; // infiniband, ethernet, fiber
    private double totalBandwidth; // Mbit/s
    private double currentAvailableBandwidth;

    /**
     * @return the networkType
     */
    public String getNetworkType() {
	return networkType;
    }

    /**
     * @param networkType
     *            the networkType to set
     */
    public void setNetworkType(String networkType) {
	this.networkType = networkType;
    }

    /**
     * @return the totalBandwidth
     */
    public double getTotalBandwidth() {
	return totalBandwidth;
    }

    /**
     * @param totalBandwidth
     *            the totalBandwidth to set
     */
    public void setTotalBandwidth(double totalBandwidth) {
	this.totalBandwidth = totalBandwidth;
    }

    /**
     * @return the currentAvailableBandwidth
     */
    public double getCurrentAvailableBandwidth() {
	return currentAvailableBandwidth;
    }

    /**
     * @param currentAvailableBandwidth
     *            the currentAvailableBandwidth to set
     */
    public void setCurrentAvailableBandwidth(double currentAvailableBandwidth) {
	this.currentAvailableBandwidth = currentAvailableBandwidth;
    }

}
