package eu.cloudlightning.bcri.clresource;

public class Memory {
    private String model;
    private String manufacturer;
    private double clockRate; // MHz
    private int busWidth; // 16-bit, 32-bit, 64-bit, 128-bit
    private double totalSize; // MB
    private double currentAvailableSize; // MB
    private String memoryType;

    public Memory() {
	this.model = "";
	this.manufacturer = "";
	this.clockRate = 0.0;
	this.busWidth = 0;
	this.totalSize = 0.0;
	this.currentAvailableSize = 0.0;
	this.memoryType = "";
    }

    /**
     * @return the model
     */
    public String getModel() {
	return model;
    }

    /**
     * @param model
     *            the model to set
     */
    public void setModel(String model) {
	this.model = model;
    }

    /**
     * @return the manufacturer
     */
    public String getManufacturer() {
	return manufacturer;
    }

    /**
     * @param manufacturer
     *            the manufacturer to set
     */
    public void setManufacturer(String manufacturer) {
	this.manufacturer = manufacturer;
    }

    /**
     * @return the clockRate
     */
    public double getClockRate() {
	return clockRate;
    }

    /**
     * @param clockRate
     *            the clockRate to set
     */
    public void setClockRate(double clockRate) {
	this.clockRate = clockRate;
    }

    /**
     * @return the busWidth
     */
    public int getBusWidth() {
	return busWidth;
    }

    /**
     * @param busWidth
     *            the busWidth to set
     */
    public void setBusWidth(int busWidth) {
	this.busWidth = busWidth;
    }

    /**
     * @return the totalSize
     */
    public double getTotalSize() {
	return totalSize;
    }

    /**
     * @param totalSize
     *            the totalSize to set
     */
    public void setTotalSize(double totalSize) {
	this.totalSize = totalSize;
    }

    /**
     * @return the currentAvailableSize
     */
    public double getCurrentAvailableSize() {
	return currentAvailableSize;
    }

    /**
     * @param currentAvailableSize
     *            the currentAvailableSize to set
     */
    public void setCurrentAvailableSize(double currentAvailableSize) {
	this.currentAvailableSize = currentAvailableSize;
    }

    /**
     * @return the memoryType
     */
    public String getMemoryType() {
	return memoryType;
    }

    /**
     * @param memoryType
     *            the memoryType to set
     */
    public void setMemoryType(String memoryType) {
	this.memoryType = memoryType;
    }

}
