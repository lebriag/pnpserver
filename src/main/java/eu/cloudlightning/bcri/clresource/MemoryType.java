package eu.cloudlightning.bcri.clresource;

public enum MemoryType {
    FPM, EDO, SDRAM, RDRAM, DDR_SDRAM, DDR2_SDRAM, DDR3_SDRAM, DDR4_SDRAM;

}
