
package eu.cloudlightning.bcri.clresource;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "authEndpoint",
    "domain",
    "password",
    "platform",
    "project",
    "sshKey",
    "username"
})
public class ResourceDescriptor {

    @JsonProperty("authEndpoint")
    private String authEndpoint;
    @JsonProperty("domain")
    private String domain;
    @JsonProperty("password")
    private String password;
    @JsonProperty("platform")
    private String platform;
    @JsonProperty("project")
    private String project;
    @JsonProperty("sshKey")
    private String sshKey;
    @JsonProperty("username")
    private String username;
//    @JsonIgnore
//    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public ResourceDescriptor() {
    }

    /**
     * 
     * @param project
     * @param platform
     * @param authEndpoint
     * @param username
     * @param sshKey
     * @param domain
     * @param password
     */
    public ResourceDescriptor(String authEndpoint, String domain, String password, String platform, String project, String sshKey, String username) {
        super();
        this.authEndpoint = authEndpoint;
        this.domain = domain;
        this.password = password;
        this.platform = platform;
        this.project = project;
        this.sshKey = sshKey;
        this.username = username;
    }

    @JsonProperty("authEndpoint")
    public String getAuthEndpoint() {
        return authEndpoint;
    }

    @JsonProperty("authEndpoint")
    public void setAuthEndpoint(String authEndpoint) {
        this.authEndpoint = authEndpoint;
    }

    @JsonProperty("domain")
    public String getDomain() {
        return domain;
    }

    @JsonProperty("domain")
    public void setDomain(String domain) {
        this.domain = domain;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("platform")
    public String getPlatform() {
        return platform;
    }

    @JsonProperty("platform")
    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @JsonProperty("project")
    public String getProject() {
        return project;
    }

    @JsonProperty("project")
    public void setProject(String project) {
        this.project = project;
    }

    @JsonProperty("sshKey")
    public String getSshKey() {
        return sshKey;
    }

    @JsonProperty("sshKey")
    public void setSshKey(String sshKey) {
        this.sshKey = sshKey;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }
//
//    @JsonAnyGetter
//    public Map<String, Object> getAdditionalProperties() {
//        return this.additionalProperties;
//    }
//
//    @JsonAnySetter
//    public void setAdditionalProperty(String name, Object value) {
//        this.additionalProperties.put(name, value);
//    }

}
