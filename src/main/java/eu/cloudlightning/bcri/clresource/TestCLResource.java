package eu.cloudlightning.bcri.clresource;

import java.io.File;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.cloudlightning.bcri.event.EventResourceRegistration;

public class TestCLResource {
    public static void main(String[] args) {
	ObjectMapper objectMapper = new ObjectMapper();
	File eResRegiFile = new File("EeventResourceRegistration.json");

	try {
	    System.out.println("reading JSON");
	    EventResourceRegistration brt = objectMapper.readValue(eResRegiFile, EventResourceRegistration.class);
	    System.out.println(brt.getClResourceType());
	    System.out.println(brt.getEntityId());
	    System.out.println(brt.getEventType());
	    System.out.println(brt.getTimestamp());

	    CLResource clRes = brt.getClResource();
	    System.out.println("\t" + clRes.getClResourceId());
	    System.out.println("\t" + clRes.getResourceDescriptor());
	    System.out.println("\t" + clRes.getResourceType());
	    System.out.println("\t" + clRes.getTelemetryEndpoint());
	    System.out.println("\t" + clRes.getTimestamp());

	    for (GenericResourceUnit gru : clRes.getGenericResourceUnits()) {
		System.out.println("\t\t" + gru.getGenericResourceUnitId());
		System.out.println("\t\t" + gru.getIpAddress());
		System.out.println("\t\t" + gru.getOperatingSystem());
		System.out.println("\t\t" + gru.getOperatingSystemVersion());
		System.out.println("\t\t" + gru.getServerName());

		ServerAccess sa = gru.getServerAccess();
		System.out.println("\t\t\t" + sa.getUsername());
		System.out.println("\t\t\t" + sa.getPassword());
		System.out.println("\t\t\t" + sa.getAccessKey());

		CPU cpu = gru.getCpu();
		System.out.println("\t\t" + cpu.getModel());
		System.out.println("\t\t" + cpu.getManufacturer());
		System.out.println("\t\t" + cpu.getProcessorType());
		System.out.println("\t\t" + cpu.getBusWidth());
		System.out.println("\t\t" + cpu.getCacheSize());
		System.out.println("\t\t" + cpu.getCurrentAvailableCores());
		System.out.println("\t\t" + cpu.getFrequency());
		System.out.println("\t\t" + cpu.getTotalNumOfCores());
		System.out.println("\t\t" + cpu.getTotalNumOfProcessor());

		Memory mem = gru.getMemory();
		System.out.println("\t\t" + mem.getManufacturer());
		System.out.println("\t\t" + mem.getMemoryType());
		System.out.println("\t\t" + mem.getModel());
		System.out.println("\t\t" + mem.getBusWidth());
		System.out.println("\t\t" + mem.getClockRate());
		System.out.println("\t\t" + mem.getCurrentAvailableSize());
		System.out.println("\t\t" + mem.getTotalSize());

		for (Network net : gru.getNetwork()) {
		    System.out.println("\t\t\t" + net.getNetworkType());
		    System.out.println("\t\t\t" + net.getCurrentAvailableBandwidth());
		    System.out.println("\t\t\t" + net.getTotalBandwidth());
		}

		for (Accelerator acc : gru.getAccelerator()) {
		    System.out.println("\t\t\t" + acc.getAcceleratorType());
		    System.out.println("\t\t\t" + acc.getBusType());
		    System.out.println("\t\t\t" + acc.getManufacturer());
		    System.out.println("\t\t\t" + acc.getMemoryType());
		    System.out.println("\t\t\t" + acc.getModel());
		    System.out.println("\t\t\t" + acc.getFrequency());
		    System.out.println("\t\t\t" + acc.getMemoryClock());
		    System.out.println("\t\t\t" + acc.getMemorySize());
		    System.out.println("\t\t\t" + acc.getNumOfCores());
		}
	    }

	    // JSON Obj to string
	    String jsonMsg = "";
	    try {
		jsonMsg = objectMapper.writeValueAsString(brt);
		System.out.println(jsonMsg);
	    } catch (JsonProcessingException e) {
		e.printStackTrace();
	    }

	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }
}
