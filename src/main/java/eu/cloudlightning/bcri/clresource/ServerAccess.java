package eu.cloudlightning.bcri.clresource;

public class ServerAccess {
    private String username;
    private String password;
    private String accessKey;

    public ServerAccess() {
	this.username = "";
	this.password = "";
	this.accessKey = "";
    }

    /**
     * @return the username
     */
    public String getUsername() {
	return username;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(String username) {
	this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
	return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
	this.password = password;
    }

    /**
     * @return the accessKey
     */
    public String getAccessKey() {
	return accessKey;
    }

    /**
     * @param accessKey
     *            the accessKey to set
     */
    public void setAccessKey(String accessKey) {
	this.accessKey = accessKey;
    }

}
