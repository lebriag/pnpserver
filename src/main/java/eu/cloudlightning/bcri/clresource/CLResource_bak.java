package eu.cloudlightning.bcri.clresource;

import java.util.ArrayList;
import java.util.List;

import eu.cloudlightning.bcri.blueprint.ProcessingStatus;
import eu.cloudlightning.bcri.blueprint.ResourceType;

public class CLResource_bak {
    private String clResourceId;
    private long timestamp;
    private String resourceType;
    private String resourceDescriptor;
    private String telemetryEndpoint;
    private String occupationalStaus;
    private List<GenericResourceUnit> genericResourceUnits;

    public CLResource_bak() {
	this.clResourceId = "";
	this.timestamp = 0;
	this.resourceType = ResourceType.OPENSTACK_ACCOUNT.name();
	this.resourceDescriptor = "";
	this.telemetryEndpoint = "";
	this.genericResourceUnits = new ArrayList<GenericResourceUnit>();
	this.occupationalStaus = ProcessingStatus.FREE.name();
    }

    /**
     * @return the clResourceId
     */
    public String getClResourceId() {
	return clResourceId;
    }

    /**
     * @param clResourceId
     *            the clResourceId to set
     */
    public void setClResourceId(String clResourceId) {
	this.clResourceId = clResourceId;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
	return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(long timestamp) {
	this.timestamp = timestamp;
    }

    /**
     * @return the resourceType
     */
    public String getResourceType() {
	return resourceType;
    }

    /**
     * @param resourceType
     *            the resourceType to set
     */
    public void setResourceType(String resourceType) {
	this.resourceType = resourceType;
    }

    /**
     * @return the resourceDescriptor
     */
    public String getResourceDescriptor() {
	return resourceDescriptor;
    }

    /**
     * @param resourceDescriptor
     *            the resourceDescriptor to set
     */
    public void setResourceDescriptor(String resourceDescriptor) {
	this.resourceDescriptor = resourceDescriptor;
    }

    /**
     * @return the telemetryEndpoint
     */
    public String getTelemetryEndpoint() {
	return telemetryEndpoint;
    }

    /**
     * @param telemetryEndpoint
     *            the telemetryEndpoint to set
     */
    public void setTelemetryEndpoint(String telemetryEndpoint) {
	this.telemetryEndpoint = telemetryEndpoint;
    }

    /**
     * @return the genericResourceUnits
     */
    public List<GenericResourceUnit> getGenericResourceUnits() {
	return genericResourceUnits;
    }

    /**
     * @param genericResourceUnits
     *            the genericResourceUnits to set
     */
    public void setGenericResourceUnits(List<GenericResourceUnit> genericResourceUnits) {
	this.genericResourceUnits = genericResourceUnits;
    }

    /**
     * @return the occupationalStaus
     */
    public String getOccupationalStaus() {
	return occupationalStaus;
    }

    /**
     * @param occupationalStaus
     *            the occupationalStaus to set
     */
    public void setOccupationalStaus(String occupationalStaus) {
	this.occupationalStaus = occupationalStaus;
    }

}
