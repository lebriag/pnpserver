package eu.cloudlightning.bcri.blueprint;

public enum ProcessingStatus {
    SUCCESSFUL, FAILED, PROCESSING, COMPLETED, UNKNOWN, START, END, PAUSED, PENDING, BUSY, FREE, FINISHED;
}
