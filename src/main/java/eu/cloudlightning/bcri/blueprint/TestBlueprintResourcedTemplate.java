package eu.cloudlightning.bcri.blueprint;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TestBlueprintResourcedTemplate {
    public static void main(String[] args) {
	ObjectMapper objectMapper = new ObjectMapper();
	File brtFile = new File("BlueprintResourcedTemplate2.json");
	try {

	    BlueprintResourcedTemplate brt = objectMapper.readValue(brtFile, BlueprintResourcedTemplate.class);

	    for (ResourcedServiceElement rse : brt.getResourcedServiceElements()) {
		System.out.println(rse.getServiceElementId());
		// for (Resources res : rse.getResources()) {
		// ObjectMapper om = new ObjectMapper();
		// if (res.getResourceType().equalsIgnoreCase("OPENSTACK")) {
		// OpenStackAccountResource osr =
		// om.readValue(res.getResourceDescriptor(),
		// OpenStackAccountResource.class);
		// System.out.println(osr.getDomain() + " " +
		// osr.getAuthEndpoint());
		// } else if
		// (res.getResourceType().equalsIgnoreCase("BAREMETAL")) {
		// BareMetalResource bmr =
		// om.readValue(res.getResourceDescriptor(),
		// BareMetalResource.class);
		// System.out.println(bmr.getIpAddress() + " " +
		// bmr.getSshKey());
		// }
		// }
	    }

	    System.out.println("\n\n" + brt.getBlueprintId() + "  " + brt.getTimestamp());
	    System.out.println(brt.getResourcedServiceElements().get(0).getResources().get(0).getResourceDescriptor());
	    System.out.println(brt.getResourcedServiceElements().get(1).getResources().get(0).getResourceDescriptor());
	    System.out.println(brt.getResourcedServiceElements().get(1).getResources().get(1).getResourceDescriptor());

	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

}
