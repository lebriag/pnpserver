package eu.cloudlightning.bcri.blueprint;

/*
 * Sent by CellManager and received by pRouters
 */
public class ResourceRequestElement {
    private String blueprintId;
    private String serviceElementId;
    private Implementation implementation;
    private long timestamp;

    public ResourceRequestElement() {
	this.blueprintId = "";
	this.serviceElementId = "";
	this.implementation = new Implementation();
	this.timestamp = 0;
    }

    /**
     * @return the blueprintId
     */
    public String getBlueprintId() {
	return blueprintId;
    }

    /**
     * @param blueprintId
     *            the blueprintId to set
     */
    public void setBlueprintId(String blueprintId) {
	this.blueprintId = blueprintId;
    }

    /**
     * @return the serviceElementId
     */
    public String getServiceElementId() {
	return serviceElementId;
    }

    /**
     * @param serviceElementId
     *            the serviceElementId to set
     */
    public void setServiceElementId(String serviceElementId) {
	this.serviceElementId = serviceElementId;
    }

    /**
     * @return the implementation
     */
    public Implementation getImplementation() {
	return implementation;
    }

    /**
     * @param implementation
     *            the implementation to set
     */
    public void setImplementation(Implementation implementation) {
	this.implementation = implementation;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
	return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(long timestamp) {
	this.timestamp = timestamp;
    }

}
