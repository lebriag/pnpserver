package eu.cloudlightning.bcri.blueprint;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TestBlueprintResourceTemplate {
    public static void main(String[] args) {
	ObjectMapper objectMapper = new ObjectMapper();
	File brtFile = new File("BlueprintResourceTemplate.json");
	try {

	    BlueprintResourceTemplate brt = objectMapper.readValue(brtFile, BlueprintResourceTemplate.class);
	    System.out.println(brt.getBlueprintId() + "\n" + brt.getServiceElements().size() + "\n"
		    + brt.getServiceElements().get(0).getImplementations().get(0).getImplementationType());
	    System.out.println(brt.getBlueprintId() + "\n" + brt.getServiceElements().size() + "\n"
		    + brt.getServiceElements().get(1).getImplementations().get(1).getMemoryRange());

	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

}
