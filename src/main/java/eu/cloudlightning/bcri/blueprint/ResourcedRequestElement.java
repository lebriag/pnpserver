package eu.cloudlightning.bcri.blueprint;

public class ResourcedRequestElement {
    private String blueprintId;
    private String status;
    private ResourcedServiceElement resourcedServiceElement;

    public ResourcedRequestElement() {
	this.blueprintId = "";
	this.status = ProcessingStatus.SUCCESSFUL.name();
	this.resourcedServiceElement = new ResourcedServiceElement();
    }

    /**
     * @return the blueprintId
     */
    public String getBlueprintId() {
	return blueprintId;
    }

    /**
     * @param blueprintId
     *            the blueprintId to set
     */
    public void setBlueprintId(String blueprintId) {
	this.blueprintId = blueprintId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
	return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
	this.status = status;
    }

    /**
     * @return the resourcedServiceElement
     */
    public ResourcedServiceElement getResourcedServiceElement() {
	return resourcedServiceElement;
    }

    /**
     * @param resourcedServiceElement
     *            the resourcedServiceElement to set
     */
    public void setResourcedServiceElement(ResourcedServiceElement resourcedServiceElement) {
	this.resourcedServiceElement = resourcedServiceElement;
    }

}
