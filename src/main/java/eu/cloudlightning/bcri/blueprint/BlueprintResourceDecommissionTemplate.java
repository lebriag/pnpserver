package eu.cloudlightning.bcri.blueprint;

public class BlueprintResourceDecommissionTemplate {
    private String blueprintId;
    private long timestamp;
    private ResourcedServiceElement resourcedServiceElement;

    public BlueprintResourceDecommissionTemplate() {
	this.blueprintId = "";
	this.timestamp = 0;
	resourcedServiceElement = new ResourcedServiceElement();
    }

    /**
     * @return the blueprintId
     */
    public String getBlueprintId() {
	return blueprintId;
    }

    /**
     * @param blueprintId
     *            the blueprintId to set
     */
    public void setBlueprintId(String blueprintId) {
	this.blueprintId = blueprintId;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
	return timestamp;
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(long timestamp) {
	this.timestamp = timestamp;
    }

    /**
     * @return the resourcedServiceElement
     */
    public ResourcedServiceElement getResourcedServiceElement() {
	return resourcedServiceElement;
    }

    /**
     * @param resourcedServiceElement
     *            the resourcedServiceElement to set
     */
    public void setResourcedServiceElement(ResourcedServiceElement resourcedServiceElement) {
	this.resourcedServiceElement = resourcedServiceElement;
    }

}
