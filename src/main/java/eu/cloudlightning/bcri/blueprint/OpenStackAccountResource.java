/*
 * Copyright 2015-17 (C) <BCRI, University College Cork>
 * 
 * Author     : Dr. Dapeng Dong
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * VERSION     AUTHOR/      DESCRIPTION OF CHANGE
 * OLD/NEW     DATE                RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0  | Dr. Dapeng Dong   | Initial Create.
 * --/1.1  | Paul Stack        | Added sshKey properties.
 *---------|-------------------|----------------------------------------------
 */

package eu.cloudlightning.bcri.blueprint;

public class OpenStackAccountResource {
	private String platform;
	private String domain;
	private String project;
	private String username;
	private String password;
	private String authEndpoint;
	private String sshKey;

	public OpenStackAccountResource() {
		this.platform = ResourceType.OPENSTACK_ACCOUNT.name();
		this.domain = "";
		this.project = "";
		this.username = "";
		this.password = "";
		this.authEndpoint = "";
		this.sshKey = "";
	}

	/**
	 * @return the platform
	 */
	public String getPlatform() {
		return platform;
	}

	/**
	 * @param platform
	 *            the platform to set
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}

	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * @param domain
	 *            the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * @return the project
	 */
	public String getProject() {
		return project;
	}

	/**
	 * @param project
	 *            the project to set
	 */
	public void setProject(String project) {
		this.project = project;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the authEndpoint
	 */
	public String getAuthEndpoint() {
		return authEndpoint;
	}

	/**
	 * @param authEndpoint
	 *            the authEndpoint to set
	 */
	public void setAuthEndpoint(String authEndpoint) {
		this.authEndpoint = authEndpoint;
	}

	/**
	 * @return the sshKey
	 */
	public String getSshKey() {
		return sshKey;
	}

	/**
	 * @param platform
	 *            the sshKey to set
	 */
	public void setSshKey(String sshKey) {
		this.sshKey = sshKey;
	}

}
