/*
 * Copyright 2015-17 (C) <BCRI, University College Cork>
 * 
 * Author     : Dr. Dapeng Dong
 *
 *-----------------------------------------------------------------------------
 * Revision History (Release 1.0.0.0)
 *-----------------------------------------------------------------------------
 * VERSION     AUTHOR/      DESCRIPTION OF CHANGE
 * OLD/NEW     DATE                RFC NO
 *-----------------------------------------------------------------------------
 * --/1.0  | Dr. Dapeng Dong   | Initial Create.
 *---------|-------------------|----------------------------------------------
 */

package eu.cloudlightning.bcri.blueprint;

public enum ResourceType {
	OPENSTACK_ACCOUNT, OPENSTACK_RESOURCE, OPENSTACK_VM, BAREMETAL, MARATHON_RESOURCE_MANAGER, ROCKS_RESOURCE_MANAGER, NUMA_RESOURCE_MANAGER, DFE_RESOURCE_MANAGER, UNKNOWN;
}
